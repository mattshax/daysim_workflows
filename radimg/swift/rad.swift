type file;

# APP CONFIGURATIONS
app (file img, file out, file err) runRadiance (file runRad, string day, string time, file rad, file runFiles[] )
{
    bash @runRad day time @rad @img stdout=@out stderr=@err;
}

# OUTPUT DEFINITIONS
string outpath=arg("out","output");

# RADIANCE SHELL SCRIPT
file runRadFile <single_file_mapper; file="bin/runRadiance.sh">;

# INPUT FILES FOR RADIANCE RUN
file rad <single_file_mapper; file=arg("rad","bin/box.rad")>;
string runDays[] = readData(arg("days","bin/days.txt"));
string runTimes[] = readData(arg("times","bin/times.txt"));
file runFiles[] <filesys_mapper;location="bin">;

foreach runday,i in runDays {
    foreach runtime,j in runTimes {
    
        string day =  regexp(runday, " ","-");
        string time =  regexp(runtime, " ","-");
        string fileid = @strjoin(["radimg",day,time],"_");
        
        tracef("%s\n",fileid);
    
        file rad_img  <single_file_mapper; file=strcat(outpath,"/bmp/",fileid,".bmp")>;
        file rad_out  <single_file_mapper; file=strcat(outpath,"/out/",fileid,".out")>;
        file rad_err <single_file_mapper; file=strcat(outpath,"/err/",fileid,".err")>;
        
        (rad_img, rad_out, rad_err) = runRadiance(runRadFile,runday,runtime,rad,runFiles);

    }
}
