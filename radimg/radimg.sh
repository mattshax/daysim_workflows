#! /bin/bash

# this workflow reads an .obj geometry file, and generates a radiance image of solar radiation for specified days/time of the year

echo "Running Radiance Image Workflow"

if [ "$*" == "" ]; then
    echo "No arguments provided. Please input name of geometry.obj file."
    exit 1
fi

# set the home directory and input filename
filename=$1 && homedir=$(pwd) && appdir=../../apps && rm output -R

# make random run directoryz
rundir=$RANDOM && mkdir $rundir && mkdir $rundir/bin

# convert the incoming obj file to a rad file        
$homedir/input/obj2rad -o obj -f $filename.obj > $rundir/$filename.rad

# copy the swift and radiance template files to rundir
cp input/* $rundir/bin && cp swift/* $rundir && cd $rundir

# start the swift job
swift rad.swift -rad=$filename.rad

# convert the result images to a video 
$appdir/convert "-delay" 50 output/bmp/* $filename.gif

# get the result file and cleanup the runjob
cd ../; mv $rundir/$filename.gif ./; mv $rundir/output ./
rm $rundir -R

echo "Radiance Image Workflow Complete"
echo "Result file at "$filename.gif