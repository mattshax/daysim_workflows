#!/bin/bash

# add radiance and daysim to path
PATH=/core/working_files/daysim_workflows/apps/daysim/bin:$PATH
PATH=/core/working_files/daysim_workflows/apps/radiance/bin:$PATH

mv $3 bin && cd bin

day=$1
time=$2
infile=$3
outfile=$4

# fix the second time value to run in increments of 2 hours instead of 1 (this is the way radiance picks up 1 hour)
time=$(echo $(echo $time | cut -d " " -f1) $(($(echo $time | cut -d " " -f2) + 1)))

pos="-435.82 -231.03 452.73"    # fixed camera position for now
dir="387.37 348.45 -439.65"     # fixed camerea direction for now

IFS=' ' read -ra posA <<< "$pos"
IFS=' ' read -ra dirA <<< "$dir"

GenCumulativeSky +s1 -a 61.18 -o 150 -m 135 -r -E -time $time -date $day $day box.epw > box.cal
oconv -f box_gcsky.rad boxmaterial.rad $infile > box.oct
rpict -t 15 -i -ab 1 -ad 512 -as 20 -ar 64 -aa 0.2 -vtv -vp ${posA[0]} ${posA[1]} ${posA[2]} -vd ${dirA[0]} ${dirA[1]} ${dirA[2]} -vu 0 0 1 -vh 26.24 -vv 26.99 -vs 0 -vl 0  -x 600 -y 400 box.oct > box_kwhm-2.pic
csh falsecolor2.csh -i box_kwhm-2.pic -s .4 -n 10 -l Whm-2 -mask 0.00001 > box_fc.pic
ra_bmp box_fc.pic output.bmp
mv output.bmp ../$outfile