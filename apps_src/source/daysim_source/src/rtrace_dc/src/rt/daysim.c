/**
 *  Daysim module
 *
 * @author Augustinus Topor (topor@ise.fhg.de)
 */
#include <string.h>

#include "daysim.h"
#include "standard.h"

#ifdef DAYSIM


/** default luminance of sky segment */
double daysimLuminousSkySegments= 1000.0;

/** default sort mode is modifier number */
int daysimSortMode= 1;


/** default number of specified sensors in zero */
int NumberOfSensorsInDaysimFile= 0;

/** default there are no sensor units assigned in rtrace_dc */
int *DaysimSensorUnits;


/** number of daylight coefficients */
static int daylightCoefficients= 0;

/*
 *
 */
int daysimInit( const int coefficients )
{
	daylightCoefficients= coefficients;

	return (daylightCoefficients >= 0) && (daylightCoefficients <= DAYSIM_MAX_COEFS);
}


/*
 *
 */
const int daysimGetCoefficients( DaysimCoef coef )
{
	return (const int)daylightCoefficients;
}


/*
 *
 */
void daysimCopy( DaysimCoef destin, DaysimCoef source )
{
	bcopy( source, destin, daylightCoefficients*sizeof( DaysimNumber ) );
}


/*
 *
 */
void daysimSet( DaysimCoef coef, const double value )
{
	int i;

	for( i= 0; i < daylightCoefficients; i++ )
		coef[i]= value;
}


/*
 *
 */
void daysimScale( DaysimCoef coef, const double scaling )
{
	int i;

	for( i= 0; i < daylightCoefficients; i++ )
		coef[i]= coef[i]*scaling;
}


/*
 *
 */
void daysimAdd( DaysimCoef result, DaysimCoef add )
{
	int i;

	for( i= 0; i < daylightCoefficients; i++ )
		result[i]= result[i] + add[i];
}

/*
 *
 */
void daysimMult( DaysimCoef result, DaysimCoef mult )
{
	int i;

	for( i= 0; i < daylightCoefficients; i++ )
		result[i]= result[i] * mult[i];
}




/*
 *
 */
void daysimSetCoef( DaysimCoef result, const int index, const double value )
{
	result[index]= value;
}


/*
 *
 */
void daysimAddCoef( DaysimCoef result, const int index, const double add )
{
	result[index]+= add;
}


/*
 *
 */
void daysimAddScaled( DaysimCoef result, DaysimCoef add, const double scaling )
{
	int i;

	for( i= 0; i < daylightCoefficients; i++ )
		result[i]+= add[i]*scaling;
}

/*
 *
 */
void daysimAssignScaled( DaysimCoef result, DaysimCoef source, const double scaling )
{
	int i;

	for( i= 0; i < daylightCoefficients; i++ )
		result[i]= source[i]*scaling;
}


/*
 * Computes the sky/ground patch hit by a ray in direction (dx,dy,dz)
 * according to the Tregenza sky division.
 */

const DaysimSourcePatch daysimComputePatch( FVECT dir )
{
	static DaysimSourcePatch number[8]= { 0, 30, 60, 84, 108, 126, 138, 144 };
	static double     ring_division[8]= { 30.0, 30.0, 24.0, 24.0, 18.0, 12.0, 6.0, 0.0 };
	int               ringnumber;
	DaysimSourcePatch patch;

	if( dir[2] > 0.0 ) {              // sky
		ringnumber=(int)(asin(dir[2])*15.0/PI);
		// origin of the number "15":
		// according to Tregenza, the celestial hemisphere is divided into 7 bands and
		// the zenith patch. The bands range from:
		//												altitude center
		// Band 1		0 to 12 Deg			30 patches	6
		// Band 2		12 to 24 Deg		30 patches	18
		// Band 3		24 to 36 Deg		24 patches	30
		// Band 4		36 to 48 Deg		24 patches	42
		// Band 5		48 to 60 Deg		18 patches	54
		// Band 6		60 to 72 Deg		12 patches	66
		// Band 7		72 to 84 Deg		 6 patches	78
		// Band 8		84 to 90 Deg		 1 patche	90
		// since the zenith patch is only takes 6Deg instead of 12, the arc length
		// between 0 and 90 Deg (equlas o and Pi/2) is divided into 7.5 units:
		// Therefore, 7.5 units = (int) asin(z=1)/(Pi/2)
		//				1 unit = asin(z)*(2*7.5)/Pi)
		//				1 unit = asin(z)*(15)/Pi)
		// Note that (int) always rounds to the next lowest integer
		if( dir[1] >= 0.0 )
			patch= number[ringnumber] + ring_division[ringnumber]*atan2(dir[1], dir[0])/(2.0*PI);
		else
			patch= number[ringnumber] + ring_division[ringnumber]*(atan2(dir[1], dir[0])/(2.0*PI) + 1.0);
	} else {                      // ground
		if( dir[2] >= -0.17365 ) {
			patch= 145;
		} else if( dir[2] >= -0.5 ) {
			patch= 146;
		} else {
			patch= 147;
		}
	}

	return patch;
}

//new version with a single ground DC
const DaysimSourcePatch daysimComputePatchDDS( FVECT dir )
{
	static DaysimSourcePatch number[8]= { 0, 30, 60, 84, 108, 126, 138, 144 };
	static double     ring_division[8]= { 30.0, 30.0, 24.0, 24.0, 18.0, 12.0, 6.0, 0.0 };
	int               ringnumber;
	DaysimSourcePatch patch;

	if( dir[2] > 0.0 ) {              // sky
		ringnumber=(int)(asin(dir[2])*15.0/PI);
		// origin of the number "15":
		// according to Tregenza, the celestial hemisphere is divided into 7 bands and
		// the zenith path. The bands range from:
		//												altitude center
		// Band 1		0 to 12 Deg			30 patches	6
		// Band 2		12 to 24 Deg		30 patches	18
		// Band 3		24 to 36 Deg		24 patches	30
		// Band 4		36 to 48 Deg		24 patches	42
		// Band 5		48 to 60 Deg		18 patches	54
		// Band 6		60 to 72 Deg		12 patches	66
		// Band 7		72 to 84 Deg		 6 patches	78
		// Band 8		84 to 90 Deg		 1 patche	90
		// since the zenith patch is only takes 6Deg instead of 12, the arc length
		// between 0 and 90 Deg (equlas o and Pi/2) is divided into 7.5 units:
		// Therefore, 7.5 units = (int) asin(z=1)/(Pi/2)
		//				1 unit = asin(z)*(2*7.5)/Pi)
		//				1 unit = asin(z)*(15)/Pi)
		// Note that (int) always rounds to the next lowest integer
		if( dir[1] >= 0.0 )
			patch= number[ringnumber] + ring_division[ringnumber]*atan2(dir[1], dir[0])/(2.0*PI);
		else
			patch= number[ringnumber] + ring_division[ringnumber]*(atan2(dir[1], dir[0])/(2.0*PI) + 1.0);
	} else {                      // ground
		patch= 145;
	}

	return patch;
}

#endif

