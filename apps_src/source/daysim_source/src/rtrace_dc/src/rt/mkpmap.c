/* 
   =============================================
   Photon map generator
   
   Roland Schregle (schregle@ise.fhg.de),
   Fraunhofer Institute for Solar Energy Systems
   =============================================
*/



#include "pmap.h"
#include "paths.h"
#include "ambient.h"
#include <string.h>
#include <sys/stat.h>



extern char VersionID [];



char* progname;                         /* argv[0] */
int  dimlist [MAXDIM];                  /* sampling dimensions */
int  ndims = 0;                         /* number of sampling dimensions */
char* octname = NULL;                   /* octree name */
CUBE thescene;                          /* THEIR scene */
OBJECT nsceneobjs;                      /* number of objects in scene */
double srcsizerat = 0.01;               /* source partition size ratio */
int backvis = 1;                        /* back face visibility */
COLOR cextinction = BLKCOLOR;           /* global extinction coefficient */
COLOR salbedo = BLKCOLOR;               /* global scattering albedo */
double seccg = 0;                       /* global scattering eccentricity */
int ambincl = -1;                       /* photon port flag */
char *amblist [128];                    /* photon port list */
char *diagFile = NULL;                  /* diagnostics output file */



/* Dummies for linkage */

COLOR ambval = BLKCOLOR;
double shadthresh = .05, ambacc = 0.2, shadcert = .5, minweight = 5e-3, 
       ssampdist = 0, dstrsrc = 0.0, specthresh = 0.15, specjitter = 1.0,
       avgrefl = 0.5;    
int ambvwt = 0, ambssamp = 0, ambres = 32, ambounce = 0, directrelay = 1,
    directvis = 1, samplendx, do_irrad = 0, ambdiv = 128, vspretest = 512,
    maxdepth = 6;
char *shm_boundary = NULL, *ambfile = NULL;
void (*trace)() = NULL, (*addobjnotify [])() = {ambnotify, NULL};


  
void printdefaults()
/* print default values to stdout */
{
   puts("-apg <file> <nPhotons>\t\t\t# global photon map");
   printf("-apm %d\t\t\t\t# max photon bounces\n", photonMaxBounce);
   puts("-app <file> <nPhotons> <bwidth>\t\t# precomputed global photon map");
   puts("-appb <file> <nPhotons> <minBw> <maxBw>\t"
        "# precomp bias comp global pmap");
   printf("-appt %f\t\t\t\t# global photon irradiance threshold\n",
          globalPmapParams.irradThresh);
   puts("-apc <file> <nPhotons>\t\t\t# caustic photon map");          
   puts("-apv <file> <nPhotons>\t\t\t# volume photon map");
   puts("-apd <file> <nPhotons>\t\t\t# direct photon map");
   printf("-apD %f\t\t\t\t# predistrib factor\n", preDistrib);
   printf("-apM %d\t\t\t\t\t# max number of predistrib passes\n", 
          maxPreDistrib);
   printf("-apf %f\t\t\t\t# global photon precomputation\n", finalGather);
   printf("-apo %s\t\t\t\t# photon port\n", NULL);
   printf("-apO %s\t\t\t\t# photon port file\n", NULL);
   printf(backvis ? "-bv+\t\t\t\t\t# back face visibility on\n" :
                    "-bv-\t\t\t\t\t# back face visibility off\n");
   printf("-dp  %f\t\t\t# PDF samples / sr\n", pdfSamples);
   printf("-ds  %f\t\t\t\t# source partition size ratio\n", srcsizerat);
   printf("-e   %s\t\t\t\t# diagnostics output file\n", diagFile);
   printf("-i   %-9ld\t\t\t\t# photon heap size increment\n", 
          photonHeapSizeInc);
   printf("-me  %.2e %.2e %.2e\t\t# extinction coefficient\n", 
          colval(cextinction,RED), colval(cextinction,GRN), 
          colval(cextinction,BLU));
   printf("-ma  %f %f %f\t\t# scattering albedo\n", colval(salbedo,RED),
          colval(salbedo,GRN), colval(salbedo,BLU));
   printf("-mg  %f\t\t\t\t# scattering eccentricity\n", seccg);
   printf("-t   %-9d\t\t\t\t# time between reports\n", photonRepTime);
}




main (int argc, char* argv [])
{
   #define check(ol, al) if (argv [i][ol] || \
                             badarg(argc - i - 1,argv + i + 1, al)) \
                            goto badopt
   #define bool(olen, var) switch (argv [i][olen]) { \
                             case '\0': var = !var; break; \
                             case 'y': case 'Y': case 't': case 'T': \
                             case '+': case '1': var = 1; break; \
                             case 'n': case 'N': case 'f': case 'F': \
                             case '-': case '0': var = 0; break; \
                             default: goto badopt; \
                          }   

   int loadflags = IO_CHECK | IO_SCENE | IO_TREE | IO_BOUNDS, rval, i;
   char *cmd, **amblp;
   unsigned cmdlen, argcnt;
   struct stat pmstat;

   /* global program name */
   progname = fixargv0(argv [0]);
   /* initialize object types */
   initotypes();
   /* opshunns */
   for (i = 1; i < argc; i++) {
      /* eggs-pand arguments */
      while (rval = expandarg(&argc, &argv, i))
         if (rval < 0) {
            sprintf(errmsg, "cannot eggs-pand '%s'", argv [i]);
            error(SYSTEM, errmsg);
         }
      if (argv[i] == NULL) break;
      if (!strcmp(argv [i], "-version")) {
         puts(VersionID);
         quit(0);
      }
      if (!strcmp(argv [i], "-defaults") || !strcmp(argv [i], "-help")) {
         printdefaults();
         quit(0);
      }
      /* get octree */
      if (i == argc - 1) {
         octname = argv [i];
         break;
      }
      switch (argv [i][1]) {
         case 'a': if (!strcmp(argv [i] + 2, "pg")) {
                      /* global photon map */
                      check(4, "ss");
                      globalPmapParams.fileName = argv [++i];
                      globalPmapParams.distribTarget = 
                         parseMultiplier(argv [++i]);
                      if (!globalPmapParams.distribTarget) goto badopt;
                      globalPmapParams.minGather = 
                         globalPmapParams.maxGather = 0;
                   }
                   else if (!strcmp(argv [i] + 2, "pm")) {
                      /* Max photon bounces */
                      check(4, "i");
                      photonMaxBounce = atol(argv [++i]);
                      if (!photonMaxBounce) goto badopt;
                   }
                   else if (!strcmp(argv [i] + 2, "pp")) {
                      /* precomputed global photon map */
                      check(4, "ssi");
                      globalPmapParams.fileName = argv [++i];
                      globalPmapParams.distribTarget = 
                         parseMultiplier(argv [++i]);
                      if (!globalPmapParams.distribTarget) goto badopt;
                      globalPmapParams.minGather = 
                         globalPmapParams.maxGather = atoi(argv [++i]);
                      if (!globalPmapParams.minGather || 
                          !globalPmapParams.maxGather) 
                         goto badopt;
                   }
                   else if (!strcmp(argv [i] + 2, "ppb")) {
                      /* precomputed global photon map + bias comp. */
                      check(5, "ssii");
                      globalPmapParams.fileName = argv [++i];
                      globalPmapParams.distribTarget = 
                         parseMultiplier(argv [++i]);
                      if (!globalPmapParams.distribTarget) goto badopt;                      
                      globalPmapParams.minGather = atoi(argv [++i]);
                      globalPmapParams.maxGather = atoi(argv [++i]);
                      if (!globalPmapParams.minGather ||
                          globalPmapParams.minGather >= 
                          globalPmapParams.maxGather) 
                         goto badopt;
                   }
                   else if (!strcmp(argv [i] + 2, "ppt")) {
                      /* precomputed global irradiance threshold */
                      check(5, "f");
                      globalPmapParams.irradThresh = atof(argv [++i]);
                      if (globalPmapParams.irradThresh <= 0) goto badopt;
                   }
                   else if (!strcmp(argv [i] + 2, "pc")) {
                      /* caustic photon map */
                      check(4, "ss");
                      causticPmapParams.fileName = argv [++i];
                      causticPmapParams.distribTarget = 
                         parseMultiplier(argv [++i]);
                      if (!causticPmapParams.distribTarget) goto badopt;
                   }
                   else if (!strcmp(argv [i] + 2, "pv")) {
                      /* volume photon map */
                      check(4, "ss");
                      volumePmapParams.fileName = argv [++i];
                      volumePmapParams.distribTarget = 
                         parseMultiplier(argv [++i]);
                      if (!volumePmapParams.distribTarget) goto badopt;                      
                   }
                   else if (!strcmp(argv [i] + 2, "pd")) {
                      /* direct photon map */
                      check(4, "ss");
                      directPmapParams.fileName = argv [++i];
                      directPmapParams.distribTarget = 
                         parseMultiplier(argv [++i]);
                      if (!directPmapParams.distribTarget) goto badopt;
                   }
                   else if (!strcmp(argv [i] + 2, "pD")) {
                      /* predistribution factor */
                      check(4, "f");
                      preDistrib = atof(argv [++i]);
                      if (preDistrib <= 0)
                         error(USER, "predistribution factor must be > 0");
                   }   
                   else if (!strcmp(argv [i] + 2, "pM")) {
                      /* max number of predistribution passes */
                      check(4, "i");
                      maxPreDistrib = atoi(argv [++i]);
                      if (!maxPreDistrib)
                         error(USER, "max number of predistribution "
                               "passes must be > 0");
                   }                
                   else if (!strcmp(argv [i] + 2, "pf")) {
                      /* final gather */
                      check(4, "f");
                      finalGather = atof(argv [++i]);
                      if (finalGather <= 0 || finalGather > 1)
                         error(USER, "global photon precomputation "
                                     "must be in range ]0, 1]");
                   }                  
                   else if (!strcmp(argv [i] + 2, "po") ||
                            !strcmp(argv [i] + 2, "pO")) {
                      /* photon ports */
                      check(4,"s");
                      if (ambincl != 1) {
                         ambincl = 1;
                         amblp = amblist;
                      }
                      if (argv[i][3] == 'O') {	
                         /* file */
                         rval = wordfile(amblp, getpath(argv [++i],
                                         getrlibpath(), R_OK));
                         if (rval < 0) {
                             sprintf(errmsg, 
                                     "cannot open photon port file \"%s\"",
                                     argv [i]);
                             error(SYSTEM, errmsg);
                         }
                         amblp += rval;
                      } 
                      else {
                         *amblp++ = argv [++i];
                         *amblp = NULL;
                      }
                   }
                   else goto badopt;
                   break;
         case 'b': if (argv [i][2] == 'v') {
                      /* back face visibility */
                      bool(3, backvis);
                   }
                   else goto badopt;
                   break;
         case 'd': /* direct */
                   switch (argv [i][2]) {
                      case 'p': /* PDF samples */
                                check(3, "f");
                                pdfSamples = atof(argv [++i]);
                                break;
                      case 's': /* source partition size ratio */
                                check(3, "f");
                                srcsizerat = atof(argv [++i]);
                                break;
                      default: goto badopt;
                   }
                   break;
         case 'e': /* diagnostics file */
                   check(2, "s");
                   diagFile = argv [++i];
                   break;
         case 'i': /* photon heap size increment */
                   check(2, "i");
                   photonHeapSizeInc = atol(argv [++i]);
                   break;
         case 'm': /* medium */
                   switch (argv[i][2]) {
                      case 'e':	/* eggs-tinction */
                                check(3, "fff");
                                setcolor(cextinction, atof(argv [i + 1]),
                                         atof(argv [i + 2]), 
                                         atof(argv [i + 3]));
                                i += 3;
                                break;
                      case 'a':	/* albedo */
                                check(3, "fff");
                                setcolor(salbedo, atof(argv [i + 1]),
                                         atof(argv [i + 2]),
                                         atof(argv [i + 3]));
                                i += 3;
                                break;
                      case 'g':	/* eccentr. */
                                check(3, "f");
                                seccg = atof(argv [++i]);
                                break;
                      default: goto badopt;
                   }
                   break;
         case 't': /* timer */
                   check(2, "i");
                   photonRepTime = atoi(argv [++i]);
                   break;
#ifdef DAYSIM
				 case 'L':				/* choose luminance of sky segments by Tito*/
				 daysimLuminousSkySegments= atof( argv[++i] );

				 if( daysimLuminousSkySegments == 0 ) {
						sprintf( errmsg, "The parameter L must not be set to zero!\n" );
						error( USER, errmsg );
				 }
				 break;
				 case 'D':				/* way of sky segment assignment by Tito*/
				 switch (argv[i][2]) {
						case 'm':			/* sorts by modifier number */
						daysimSortMode=1;
						break;
						case 'd':			/* sorts by ray direction */
						daysimSortMode=2;
						break;
						default:
						goto badopt;
				 }
				 break;
 	
				 case 'N':				/* choose luminance of sky segments by Tito*/
				 if( daysimInit( atoi( argv[++i] ) ) == 0 ) {
						sprintf( errmsg, "The parameter N must lie between 0 and 148!\n" );
						error( USER, errmsg );
				 }
				 break;
#endif
         default: goto badopt;
      }
   }
   /* Open diagnostics file */
   if (diagFile) {
      if (!freopen(diagFile, "a", stderr)) quit(2);
      fprintf(stderr, "**************\n*** PID %5d: ", getpid());
      printargs(argc, argv, stderr);
      putc('\n', stderr);
      fflush(stderr);
   }
#ifdef NICE
   /* Lower priority */
   nice(NICE);
#endif
   setPmapParams(&globalPmap, &globalPmapParams, 
                 &causticPmap, &causticPmapParams,
                 &volumePmap, &volumePmapParams,
                 &directPmap, &directPmapParams);
   if (!globalPmap && !causticPmap && !volumePmap && !directPmap)
      error(USER, "no photon maps specified");
   if (octname == NULL) error(USER, "missing octree argument");
   /* Don't overwrite eggsisting files */
   if (globalPmap && !stat(globalPmap -> fileName, &pmstat) || 
       causticPmap && !stat(causticPmap -> fileName, &pmstat) || 
       volumePmap && !stat(volumePmap -> fileName, &pmstat) || 
       directPmap && !stat(directPmap -> fileName, &pmstat))
      error(USER, "photon map file exists, not overwritten");
   readoct(octname, loadflags, &thescene, NULL);
   nsceneobjs = nobjects;
   /* Get sources */
   marksources();
   distribPhotons(globalPmap, causticPmap, volumePmap, directPmap);
   /* Get command line */
   for (cmdlen = argcnt = 1; argcnt < argc; 
        cmdlen += strlen(argv [argcnt++]) + 1);
   cmd = (char*)malloc(strlen(progname) + cmdlen + 1);
   strcpy(cmd, progname);
   for (argcnt = 1; argcnt < argc; 
        strcat(cmd, " "), strcat(cmd, argv [argcnt++]));
   savePmaps(globalPmap, causticPmap, volumePmap, directPmap, cmd);
   cleanUpPmaps(globalPmap, causticPmap, volumePmap, directPmap);
   free(cmd);
   quit(0);

badopt:
   sprintf(errmsg, "command line error at '%s'", argv[i]);
   error(USER, errmsg);

   #undef check
   #undef bool
}
