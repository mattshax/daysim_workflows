/**
 *  Daysim header file
 *
 * @author Augustinus Topor (topor@ise.fhg.de)
 */

#ifndef DAYSIM_H
#define DAYSIM_H


#include "fvect.h"



#define DAYSIM_MAX_COEFS		148
//#define DAYSIM_MAX_COEFS		2306

/**
 * daylight coefficient
 */
typedef float DaysimNumber;

typedef DaysimNumber DaysimCoef[DAYSIM_MAX_COEFS];



/**
 *
 */
/*#ifdef PHOTON_MAP*/
/* necessary for pmap which is built no matter if PHOTON_MAP is set or not */
typedef unsigned char DaysimSourcePatch;
/*#endif*/


#ifdef DAYSIM


/** */
extern double daysimLuminousSkySegments;

/** */
extern int daysimSortMode;

/** */
extern int NumberOfSensorsInDaysimFile;

/** */

extern int *DaysimSensorUnits;

/** */
int daysimInit( const int coefficients );

/** returns the number of coefficients */
const int daysimGetCoefficients();


/** Returns a new daylight coefficient struct */
//DAYLIGHT_COEF* daysimNew();

/** Releases the memory of the daylight coefficient structure */
//void daysimDelete( DAYLIGHT_COEF* daylightCoef);


/** Copies a daylight coefficient set */
void daysimCopy( DaysimCoef destin, DaysimCoef source );

/** Initialises all daylight coefficients with 'value' */
void daysimSet( DaysimCoef coef, const double value );

/** Scales the daylight coefficient set by the value 'scaling' */
void daysimScale( DaysimCoef coef, const double scaling );

/** Adds two daylight coefficient sets:
	result[i]= result[i] + add[i] */
void daysimAdd( DaysimCoef result, DaysimCoef add );

/** Multiply two daylight coefficient sets:
	result[i]= result[i] * add[i] */
void daysimMult( DaysimCoef result, DaysimCoef mult );

/** Sets the daylight coefficient at position 'index' to 'value' */
void daysimSetCoef( DaysimCoef result, const int index, const double value );

/** Adds 'value' to the daylight coefficient at position 'index' */
void daysimAddCoef( DaysimCoef result, const int index, const double add );

/** Adds the elements of 'source' scaled by 'scaling'  to 'result' */
void daysimAddScaled( DaysimCoef result, DaysimCoef add, const double scaling );

/** Assign the coefficients of 'source' scaled by 'scaling' to result */
void daysimAssignScaled( DaysimCoef result, DaysimCoef source, const double scaling );

#ifdef PHOTON_MAP
/** Computes the sky/ground patch hit by a ray in direction (dx,dy,dz) */
const DaysimSourcePatch daysimComputePatch( FVECT dir );
const DaysimSourcePatch daysimComputePatchDDS( FVECT dir );
#endif

#define DAYSIM_COPY( res, src ) ( daysimCopy( res, src ) )
#define DAYSIM_SET( res, v ) ( daysimSet( res, v ) )
#define DAYSIM_ADD( res, dc ) ( daysimAdd( res, dc ) )
#define DAYSIM_SCALE( res, c ) ( daysimScale( res, c ) )
#define DAYSIM_MULT( a, b ) ( daysimMult( a, b ) )

#define DAYSIM_SET_COEF( res, i, v ) ( daysimSetCoef( res, i, v ) )
#define DAYSIM_ADD_COEF( res, i, v ) ( daysimAddCoef( res, i, v ) )
#define DAYSIM_ADD_SCALED( res, dc, c ) ( daysimAddScaled( res, dc, c ) )
#define DAYSIM_ASSIGN_SCALED( res, dc, c ) ( daysimAssignScaled( res, dc, c ) )

#else  /* empty defines */

#define DAYSIM_COPY( a, b )
#define DAYSIM_SET( a, b )
#define DAYSIM_ADD( a, b )
#define DAYSIM_SCALE( a, b )
#define DAYSIM_MULT( a, b )

#define DAYSIM_SET_COEF( res, i, v )
#define DAYSIM_ADD_COEF( a, b, c )
#define DAYSIM_ADD_SCALED( a, b, c )
#define DAYSIM_ASSIGN_SCALED( a, b, c )

#endif /* DAYSIM */


#endif /* DAYSIM_H */
