#ifndef lint
static const char	RCSid[] = "$Id: wordfile.c,v 2.15 2005/06/10 16:42:11 greg Exp $";
#endif
/*
 * Load whitespace separated words from a file into an array.
 * Assume the passed pointer array is big enough to hold them all.
 *
 * External symbols declared in standard.h
 */

#include "copyright.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "platform.h"
#include "standard.h"


#ifndef MAXFLEN
#define MAXFLEN		65536	/* file must be smaller than this */
#define BUFLEN		10240
#endif



int
wordfile_nl(words, fname)		/* get words from fname, put in words, no size limitation */
char	***words;
char	*fname;
{
	int		fd;
	char	*buf= NULL;
	int		buf_size= 0;
	register int	n;
	struct stat st;
					/* load file into buffer */
	if (fname == NULL)
		return(-1);			/* no filename */
	if ((fd = open(fname, 0)) < 0)
		return(-1);			/* open error */
	if( fstat( fd, &st ) != 0 )
		return -1;			/* error */

	buf_size= st.st_size + 1;
	buf= calloc( sizeof(char), buf_size );
	if ( buf == NULL )
		return -1;

	n= read(fd, buf, buf_size - 1);
	close(fd);

	if ( n < 0 )
		return -1;
	buf[n] = '\0';			/* terminate */

	if ( *words == NULL ) { /* count the words */
		int lsp= 1;
		int c= 1; /* +1 */
	
		while( n > 0 ) {
			int sp= isspace(buf[--n]);
			if( !sp && lsp )
				c++;
			lsp= sp;
		}
		*words= (char**)calloc( sizeof(char*), c );
	}

	n= wordstring(*words, buf);	/* wordstring does the rest */

	free( buf );
	return n;
}

int
wordfile(words, fname)		/* get words from fname, put in words */
char	**words;
char	*fname;
{
	return wordfile_nl( &words, fname );


	int	fd;
	char	buf[MAXFLEN];
	register int	n;
					/* load file into buffer */
	if (fname == NULL)
		return(-1);			/* no filename */
	if ((fd = open(fname, 0)) < 0)
		return(-1);			/* open error */
	n = read(fd, buf, MAXFLEN);
	close(fd);
	if (n < 0)				/* read error */
		return(-1);
	if (n == MAXFLEN)		/* file too big, take what we can */
		while (!isspace(buf[--n]))
			if (n <= 0)		/* one long word! */
				return(-1);
	buf[n] = '\0';			/* terminate */
	n= wordstring(words, buf);	/* wordstring does the rest */

	free( buf );
	return n;
}



int
wordstring(avl, str)			/* allocate and load argument list */
char	**avl;
char	*str;
{
	register char	*cp, **ap;
	
	if (str == NULL)
		return(-1);
	cp = bmalloc(strlen(str)+1);
	if (cp == NULL)			/* ENOMEM */
		return(-1);
	strcpy(cp, str);
	ap = avl;		/* parse into words */
	for ( ; ; ) {
		while (isspace(*cp))	/* nullify spaces */
			*cp++ = '\0';
		if (!*cp)		/* all done? */
			break;
		*ap++ = cp;		/* add argument to list */
		while (*++cp && !isspace(*cp))
			;
	}
	*ap = NULL;
	return(ap - avl);
}
