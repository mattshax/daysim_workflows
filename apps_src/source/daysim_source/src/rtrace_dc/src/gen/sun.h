
int jdate( int month, int day);		/* Julian date (days into year) */

double stadj( int jd);		/* solar time adjustment from Julian date */

double sdec( int jd);		/* solar declination angle from Julian date */

double salt( double sd, double st);	/* solar altitude from solar declination and solar time */

double sazi( double sd,  double st);	/* solar azimuth from solar declination and solar time */

