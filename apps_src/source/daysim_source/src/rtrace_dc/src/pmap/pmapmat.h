#ifndef PMAPMAT_H
#define PMAPMAT_H



/* 
   ==================================================================
   Photon map support routines for scattering by materials. 
   Nu stuff, babee!
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmap.h"



/* Check for redundant path already accounted for by caustic photons */
#define causticPath(ray) (causticPmap && (ray) -> crtype & AMBIENT)



/* Dispatch table for photon scattering functions */
extern int (*photonScatter []) (OBJREC*, RAY*);



extern void initPhotonScatterFuncs ();
/* Init photonScatter[] dispatch table with material specific scattering 
   routines */

#endif
