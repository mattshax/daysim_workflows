/* 
   ==================================================================
   Photon map support routines for scattering by materials. 

   Greetings to Capt. C.B.!
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapmat.h"
#include "pmapdata.h"
#include "pmaprand.h"
#include "pmapmisc.h"
#include "otypes.h"
#include "data.h"
#include "func.h"

#include <math.h>


int (*photonScatter [NUMOTYPE]) (OBJREC*, RAY*);



/* Stuff ripped off from material modules */

#define  MAXITER 10
#define  SP_REFL 01
#define  SP_TRAN 02
#define  SP_PURE 04
#define  SP_FLAT 010
#define  SP_BADU 040
#define  MLAMBDA 500
#define  RINDEX	1.52
#define  FRESNE(ci) (exp(-5.85*(ci)) - 0.00287989916)



typedef struct {
   OBJREC *mp;
   RAY *rp;
   short specfl;
   COLOR mcolor, scolor;
   FVECT vrefl, prdir, pnorm;
   double alpha2, rdiff, rspec, trans, tdiff, tspec, pdot;
}  NORMDAT;

typedef struct {
   OBJREC  *mp;
   RAY  *rp;
   short  specfl;
   COLOR  mcolor, scolor;
   FVECT  vrefl, prdir, u, v, pnorm;
   double  u_alpha, v_alpha, rdiff, rspec, trans, tdiff, tspec, pdot;
}  ANISODAT;



extern double mylog(double);
extern void getacoords (RAY*, ANISODAT*);



static int isoSpecPhotonScatter (NORMDAT* nd, RAY* rayOut)
/* Generate direction for isotropically specularly reflected 
   (if rayOut -> rtype & REFLECTED) or transmitted ray. 
   Returns 1 if successful. */
{
   FVECT u, v, h;
   double d, d2, sinp, cosp;
   int niter;
   register int i;
   RAY* rayIn = nd -> rp;
   
   /* Set up sample coordinates */  
   i = 0;
   do {
      v [0] = v [1] = v [2] = 0;
      v [i++] = 1;
      fcross(u, v, nd -> pnorm);
   } while (normalize(u) < FTINY);
   fcross(v, nd -> pnorm, u);
   if (nd -> specfl & SP_REFL) {
      /* Specular reflection */
      /* Make MAXITER attempts at getting a ray */
      for (niter = 0; niter < MAXITER; niter++) {
         d = 2 * PI * pmapRandom(scatterState);
         cosp = cos(d);
         sinp = sin(d);
         d2 = pmapRandom(scatterState);
         d = d2 <= FTINY ? 1 : sqrt(nd -> alpha2 * -log(d2));
         for (i = 0; i < 3; i++)
            h [i] = nd -> pnorm [i] + d * (cosp * u [i] + sinp * v [i]);
         d = -2 * DOT(h, rayIn -> rdir) / (1 + d * d);
         VSUM(rayOut -> rdir, rayIn -> rdir, h, d);
         if (DOT(rayOut -> rdir, rayIn -> ron) > FTINY) return 1;
      }
      return 0;
   }
   else {
      /* Specular transmission */
      /* Make MAXITER attempts at getting a ray */
      for (niter = 0; niter < MAXITER; niter++) {
         d = 2 * PI * pmapRandom(scatterState);
         cosp = cos(d);
         sinp = sin(d);
         d2 = pmapRandom(scatterState);
         d = d2 <= FTINY ? 1 : sqrt(-log(d2) * nd -> alpha2);
         for (i = 0; i < 3; i++)
            rayOut -> rdir [i] = nd -> prdir [i] + 
                                 d * (cosp * u [i] + sinp * v [i]);
         if (DOT(rayOut -> rdir, rayIn -> ron) < -FTINY) {
            normalize(rayOut -> rdir);
            return 1;
         }
      }
      return 0;
   }
}



static void diffPhotonScatter (FVECT normal, RAY* rayOut)
/* Generate cosine-weighted direction for diffuse ray */
{
   const RREAL cosThetaSqr = pmapRandom(scatterState), 
               cosTheta = sqrt(cosThetaSqr),
               sinTheta = sqrt(1 - cosThetaSqr), 
               phi = 2 * PI * pmapRandom(scatterState), 
               du = cos(phi) * sinTheta, dv = sin(phi) * sinTheta;
   FVECT u, v;
   register int i;

   /* Set up sample coordinates */
   i = 0;
   do {
      v [0] = v [1] = v [2] = 0;
      v [i++] = 1;
      fcross(u, v, normal);
   } while (normalize(u) < FTINY);
   fcross(v, normal, u);
   /* Convert theta & phi to cartesian */
   for (i = 0; i < 3; i++)
      rayOut -> rdir [i] = du * u [i] + dv * v [i] + cosTheta * normal [i];
   normalize(rayOut -> rdir);
}



static int normalPhotonScatter (OBJREC* mat, RAY* rayIn)
/* Generate new photon ray for isotropic material and recurse */
{
   NORMDAT nd;
   register int i, hastexture;
   float xi, albedo, prdiff, ptdiff, prspec, ptspec;
   double d, fresnel;
   RAY rayOut;
   //   COLOR rayWgt;

   if (mat -> oargs.nfargs != (mat -> otype == MAT_TRANS ? 7 : 5))
      objerror(mat, USER, "bad number of arguments");
   /* Check for back side; reorient if back is visible */
   if (rayIn -> rod < 0)
      if (!backvis && mat -> otype != MAT_TRANS) return;
      else {
         /* Get modifiers */
         raytexture(rayIn, mat -> omod);
         flipsurface(rayIn);
      }
   else raytexture(rayIn, mat -> omod);
   nd.rp = rayIn;
   /* Get material color */
   copycolor(nd.mcolor, mat -> oargs.farg);
   /* Get roughness */
   nd.specfl = 0;
   nd.alpha2 = mat -> oargs.farg [4];
   if ((nd.alpha2 *= nd.alpha2) <= FTINY) 
      nd.specfl |= SP_PURE;
   if (rayIn -> ro != NULL && isflat(rayIn -> ro -> otype)) 
      nd.specfl |= SP_FLAT;  
   /* Perturb normal */
   if ( (hastexture = DOT(rayIn -> pert, rayIn -> pert)) > FTINY * FTINY) {
      nd.pdot = raynormal(nd.pnorm, rayIn);
   } else {
      VCOPY(nd.pnorm, rayIn -> ron);
      nd.pdot = rayIn -> rod;
   }

   if (nd.pdot < .001)
	   nd.pdot = .001;
   /* Modify material color */
   multcolor(nd.mcolor, rayIn -> pcol);
   nd.rspec = mat -> oargs.farg [3];
   /* Approximate Fresnel term */
   if (nd.specfl & SP_PURE && nd.rspec > FTINY) {
      fresnel = FRESNE(rayIn -> rod);
      nd.rspec += fresnel * (1 - nd.rspec);
   } 
   else fresnel = 0;
   /* Transmission params */
   if (mat -> otype == MAT_TRANS) {
      nd.trans = mat -> oargs.farg [5] * (1 - nd.rspec);
      nd.tspec = nd.trans * mat -> oargs.farg [6];
      nd.tdiff = nd.trans - nd.tspec; 
   } 
   else nd.tdiff = nd.tspec = nd.trans = 0;
   /* Specular reflection params */
   if (nd.rspec > FTINY) {      
      /* Specular color */
      if (mat -> otype != MAT_METAL) 
         setcolor(nd.scolor, nd.rspec, nd.rspec, nd.rspec);
      else if (fresnel > FTINY) {
         d = nd.rspec * (1 - fresnel);
         for (i = 0; i < 3; i++) 
            nd.scolor [i] = fresnel + nd.mcolor [i] * d;
      }
      else {
         copycolor(nd.scolor, nd.mcolor);
         scalecolor(nd.scolor, nd.rspec);
      }
   }
   else setcolor(nd.scolor, 0, 0, 0);
   /* Diffuse reflection params */
   nd.rdiff = 1 - nd.trans - nd.rspec;
   /* Set up probabilities */
   prdiff = ptdiff = ptspec = colorAvg(nd.mcolor);
   prdiff *= nd.rdiff;
   ptdiff *= nd.tdiff;
   prspec = colorAvg(nd.scolor);
   ptspec *= nd.tspec;
   albedo = prdiff + ptdiff + prspec + ptspec;
   /* Insert direct and indirect photon hits if diffuse component */
   if (prdiff > FTINY || ptdiff > FTINY) {
	   if (!rayIn -> rlvl) {
		   addPhoton(directPmap, rayIn);
	   } else {
		   addPhoton(globalPmap, rayIn);
		   /* Store caustic photon if specular flag set */
		   if (rayIn -> rsrc)
			   addPhoton(causticPmap, rayIn);
	   }
   }
   xi = pmapRandom(rouletteState);
   /* Absorbed? */
   if (xi > albedo) return;
   if (xi > (albedo -= prspec)) {
      /* Specular reflection */
      nd.specfl |= SP_REFL;
      if (nd.specfl & SP_PURE) {
         /* Perfect specular reflection */
         for (i = 0; i < 3; i++)
            /* Reflected ray */
            nd.vrefl [i] = rayIn -> rdir [i] + 
                           2 * nd.pdot * nd.pnorm [i];
         /* Penetration? (X-rated code starts here) */
         if (hastexture && DOT(nd.vrefl, rayIn -> ron) <= FTINY)
            for (i = 0; i < 3; i++)
               /* Safety measure */
               nd.vrefl [i] = rayIn -> rdir [i] + 
                              2 * rayIn -> rod * rayIn -> ron [i];
         VCOPY(rayOut.rdir, nd.vrefl);
      }
      else if (!isoSpecPhotonScatter(&nd, &rayOut)) return;
      rayorigin(&rayOut, REFLECTED | SPECULAR, rayIn, NULL);
      copycolor(rayOut.rcol, nd.scolor);   
      /* Set specular flag */
      rayOut.rsrc = 1;
   }
   else if (xi > (albedo -= ptspec)) {
      /* Specular transmission */
      nd.specfl |= SP_TRAN;
      if (hastexture) {
         /* Perturb */
         for (i = 0; i < 3; i++) 
            nd.prdir [i] = rayIn -> rdir [i] - rayIn -> pert [i];
         if (DOT(nd.prdir, rayIn -> ron) < -FTINY) normalize(nd.prdir);
         else VCOPY(nd.prdir, rayIn -> rdir);
      }
      else VCOPY(nd.prdir, rayIn -> rdir);
      if ((nd.specfl & (SP_TRAN | SP_PURE)) == (SP_TRAN | SP_PURE))
         /* Perfect specular transmission */   
         VCOPY(rayOut.rdir, nd.prdir);
      else if (!isoSpecPhotonScatter(&nd, &rayOut)) return;
      rayorigin(&rayOut, TRANS | SPECULAR, rayIn, NULL);
      copycolor(rayOut.rcol, nd.mcolor);   
      /* Set specular flag */
      rayOut.rsrc = 1;
   }
   else if (xi > (albedo -= prdiff)) {
      /* Diffuse reflection */
      rayorigin(&rayOut, REFLECTED | AMBIENT, rayIn, NULL);
      copycolor(rayOut.rcol, nd.mcolor);
      /* Reset specular flag */
      rayOut.rsrc = 0;
      diffPhotonScatter(hastexture ? nd.pnorm : rayIn -> ron, &rayOut);
   }
   else {
      /* Diffuse transmission */
      flipsurface(rayIn);
      rayorigin(&rayOut, TRANS | AMBIENT, rayIn, NULL);
      copycolor(rayOut.rcol, nd.mcolor);
      /* Reset specular flag */
      rayOut.rsrc = 0;
      if (hastexture) {
         FVECT bnorm;
         bnorm [0] = -nd.pnorm [0];
         bnorm [1] = -nd.pnorm [1];
         bnorm [2] = -nd.pnorm [2];
         diffPhotonScatter(bnorm, &rayOut);
      } else diffPhotonScatter(rayIn -> ron, &rayOut);
   }
   /* Modify ray colour and normalise to maintain uniform flux */
   multcolor(rayOut.rcol, rayIn -> rcol);
   colorNorm(rayOut.rcol);
   tracePhoton(&rayOut);
}



static int anisoSpecPhotonScatter (ANISODAT* nd, RAY* rayOut)
/* Generate direction for anisotropically specularly reflected 
   (if rayOut -> rtype & REFLECTED) or transmitted ray. 
   Returns 1 if successful. */
{
   FVECT h;
   double d, d2, sinp, cosp;
   int niter;
   register int i;
   RAY* rayIn = nd -> rp;

   if (rayIn -> ro != NULL && isflat(rayIn -> ro -> otype)) 
      nd -> specfl |= SP_FLAT;
   /* set up coordinates */
   getacoords(rayIn, nd);
   if (rayOut -> rtype & REFLECTED) {
      /* Specular reflection */
      /* Make MAXITER attempts at getting a ray */
      for (niter = 0; niter < MAXITER; niter++) {
         d = 2 * PI * pmapRandom(scatterState);
         cosp = cos(d) * nd -> u_alpha;
         sinp = sin(d) * nd -> v_alpha;
         d = sqrt(cosp * cosp + sinp * sinp);
         cosp /= d;
         sinp /= d;
         d2 = pmapRandom(scatterState);
         d = d2 <= FTINY 
             ? 1 
             : sqrt(-log(d2) / 
                    (cosp * cosp / (nd -> u_alpha * nd -> u_alpha) +
                     sinp * sinp / (nd -> v_alpha * nd -> v_alpha)));
         for (i = 0; i < 3; i++)
            h [i] = nd -> pnorm [i] + 
                    d * (cosp * nd -> u [i] + sinp * nd -> v [i]);
         d = -2 * DOT(h, rayIn -> rdir) / (1 + d * d);
         VSUM(rayOut -> rdir, rayIn -> rdir, h, d);
         if (DOT(rayOut -> rdir, rayIn -> ron) > FTINY) return 1;
      }
      return 0;
   }
   else {
      /* Specular transmission */
      if (DOT(rayIn -> pert, rayIn -> pert) <= FTINY * FTINY) 
         VCOPY(nd -> prdir, rayIn -> rdir);
      else {
         /* perturb */
         for (i = 0; i < 3; i++) 
            nd -> prdir [i] = rayIn -> rdir [i] - rayIn -> pert [i];
         if (DOT(nd -> prdir, rayIn -> ron) < -FTINY) 
            normalize(nd -> prdir);
         else VCOPY(nd -> prdir, rayIn -> rdir);
      }
      /* Make MAXITER attempts at getting a ray */
      for (niter = 0; niter < MAXITER; niter++) {
         d = 2 * PI * pmapRandom(scatterState);
         cosp = cos(d) * nd -> u_alpha;
         sinp = sin(d) * nd -> v_alpha;
         d = sqrt(cosp * cosp + sinp * sinp);
         cosp /= d;
         sinp /= d;
         d2 = pmapRandom(scatterState);
         d = d2 <= FTINY
             ? 1
             : sqrt(-log(d2) /
                    (cosp * cosp / (nd -> u_alpha * nd -> u_alpha) +
                     sinp * sinp / (nd -> v_alpha * nd -> u_alpha)));
         for (i = 0; i < 3; i++)
            rayOut -> rdir [i] = nd -> prdir [i] + d * 
                                 (cosp * nd -> u [i] + sinp * nd -> v [i]);
         if (DOT(rayOut -> rdir, rayIn -> ron) < -FTINY) {
            normalize(rayOut -> rdir);
            return 1;
         }
      }
      return 0;
   }
}



static int anisoPhotonScatter (OBJREC* mat, RAY* rayIn)
/* Generate new photon ray for anisotropic material and recurse */
{
   ANISODAT nd;
   //   register int i;
   float xi, albedo, prdiff, ptdiff, prspec, ptspec;
   RAY rayOut;
   
   if (mat -> oargs.nfargs != (mat -> otype == MAT_TRANS2 ? 8 : 6))
      objerror(mat, USER, "bad number of real arguments");
   nd.rp = rayIn;
   nd.mp = objptr(rayIn -> ro -> omod);
   /* get material color */
   copycolor(nd.mcolor, mat -> oargs.farg);
   /* get roughness */
   nd.specfl = 0;
   nd.u_alpha = mat -> oargs.farg [4];
   nd.v_alpha = mat -> oargs.farg [5];
   if (nd.u_alpha < FTINY || nd.v_alpha <= FTINY)
      objerror(mat, USER, "roughness too small");
   /* check for back side; reorient if back is visible */
   if (rayIn -> rod < 0)
      if (!backvis && mat -> otype != MAT_TRANS2) return;
      else {
         /* get modifiers */
         raytexture(rayIn, mat -> omod);
         flipsurface(rayIn);
      }
   else raytexture(rayIn, mat -> omod);
   /* perturb normal */
   nd.pdot = raynormal(nd.pnorm, rayIn);
   if (nd.pdot < 0.001) nd.pdot = 0.001;
   /* modify material color */
   multcolor(nd.mcolor, rayIn -> pcol);
   nd.rspec = mat -> oargs.farg [3];
   /* transmission params */
   if (mat -> otype == MAT_TRANS2) {
      nd.trans = mat -> oargs.farg [6] * (1 - nd.rspec);
      nd.tspec = nd.trans * mat -> oargs.farg [7];
      nd.tdiff = nd.trans - nd.tspec;
      if (nd.tspec > FTINY) nd.specfl |= SP_TRAN;
   } 
   else nd.tdiff = nd.tspec = nd.trans = 0;
   /* specular reflection params */
   if (nd.rspec > FTINY) {
      nd.specfl |= SP_REFL;
      /* compute specular color */
      if (mat -> otype == MAT_METAL2) copycolor(nd.scolor, nd.mcolor);
      else setcolor(nd.scolor, 1, 1, 1);
      scalecolor(nd.scolor, nd.rspec);
   }
   else setcolor(nd.scolor, 0, 0, 0);
   /* diffuse reflection params */
   nd.rdiff = 1 - nd.trans - nd.rspec;
   /* Set up probabilities */
   prdiff = ptdiff = ptspec = colorAvg(nd.mcolor);
   prdiff *= nd.rdiff;
   ptdiff *= nd.tdiff;
   prspec = colorAvg(nd.scolor);
   ptspec *= nd.tspec;
   albedo = prdiff + ptdiff + prspec + ptspec;
   /* Insert direct and indirect photon hits if diffuse component */
   if ( prdiff > FTINY || ptdiff > FTINY ) {
	   if (!rayIn -> rlvl) {
		   addPhoton(directPmap, rayIn);
	   } else {
		   addPhoton(globalPmap, rayIn);
		   /* Store caustic photon if specular flag set */
		   if (rayIn -> rsrc)
			   addPhoton(causticPmap, rayIn);
	   }
   }
   xi = pmapRandom(rouletteState);
   /* Absorbed? */
   if (xi > albedo) return;
   if (xi > (albedo -= prspec))
      /* Specular reflection */
      if (!(nd.specfl & SP_BADU)) {
         rayorigin(&rayOut, REFLECTED | SPECULAR, rayIn, NULL);
         copycolor(rayOut.rcol, nd.scolor);
         /* Set specular flag */
         rayOut.rsrc = 1;
         if (!anisoSpecPhotonScatter(&nd, &rayOut)) return;
      }
      else return;
   else if (xi > (albedo -= ptspec))
      /* Specular transmission */
      if (!(nd.specfl & SP_BADU)) {
         /* Specular transmission */
         rayorigin(&rayOut, TRANS | SPECULAR, rayIn, NULL);
         copycolor(rayOut.rcol, nd.mcolor);
         /* Set specular flag */
         rayOut.rsrc = 1;
         if (!anisoSpecPhotonScatter(&nd, &rayOut)) return;
      }
      else return;
   else if (xi > (albedo -= prdiff)) {
      /* Diffuse reflection */   
      rayorigin(&rayOut, REFLECTED | AMBIENT, rayIn, NULL);
      copycolor(rayOut.rcol, nd.mcolor);
      /* Reset specular flag */
      rayOut.rsrc = 0;      
      diffPhotonScatter(nd.pnorm, &rayOut);
   }
   else {
      /* Diffuse transmission */
      FVECT bnorm;
      flipsurface(rayIn);
      bnorm [0] = -nd.pnorm [0];
      bnorm [1] = -nd.pnorm [1];
      bnorm [2] = -nd.pnorm [2];
      rayorigin(&rayOut, TRANS | AMBIENT, rayIn, NULL);
      copycolor(rayOut.rcol, nd.mcolor);
      /* Reset specular flag */
      rayOut.rsrc = 0;
      diffPhotonScatter(bnorm, &rayOut);
   }
   /* Modify ray colour and normalise to maintain uniform flux */
   multcolor(rayOut.rcol, rayIn -> rcol);
   colorNorm(rayOut.rcol);
   tracePhoton(&rayOut);
}



static int dielectricPhotonScatter (OBJREC* mat, RAY* rayin)
/* Generate new photon ray for dielectric material and recurse */
{
   double cos1, cos2, nratio, d1, d2, refl, trans;
   COLOR ctrans, talb;
   FVECT dnorm;
   register int i, hastexture;
   RAY rayout;

   if (mat -> oargs.nfargs != (mat -> otype == MAT_DIELECTRIC ? 5 : 8))
      objerror(mat, USER, "bad arguments");
   /* get modifiers */
   raytexture(rayin, mat -> omod);			
   if ( (hastexture = DOT(rayin -> pert, rayin -> pert)) > FTINY * FTINY)
      /* Perturb normal */
      cos1 = raynormal(dnorm, rayin);
   else {
      VCOPY(dnorm, rayin -> ron);
      cos1 = rayin -> rod;
   }
   /* index of refraction */
   nratio = mat -> otype == MAT_DIELECTRIC 
            ? mat -> oargs.farg [3] + mat -> oargs.farg [4] / MLAMBDA
            : mat -> oargs.farg [3] / mat -> oargs.farg [7];
   if (cos1 < 0) {
      /* inside */
      hastexture = -hastexture;
      cos1 = -cos1;
      dnorm [0] = -dnorm [0];
      dnorm [1] = -dnorm [1];
      dnorm [2] = -dnorm [2];            
      setcolor(rayin -> cext, 
               -mylog(mat -> oargs.farg [0] * rayin -> pcol [0]),
               -mylog(mat -> oargs.farg [1] * rayin -> pcol [1]),
               -mylog(mat -> oargs.farg [2] * rayin -> pcol [2]));
      setcolor(rayin -> albedo, 0, 0, 0);
      rayin -> gecc = 0;
      if (mat -> otype == MAT_INTERFACE) {
         setcolor(ctrans, 
                  -mylog(mat -> oargs.farg [4] * rayin -> pcol [0]),
                  -mylog(mat -> oargs.farg [5] * rayin -> pcol [1]),
                  -mylog(mat -> oargs.farg [6] * rayin -> pcol [2]));
         setcolor(talb, 0, 0, 0);
      } 
      else {
         copycolor(ctrans, cextinction);
         copycolor(talb, salbedo);
      }
   } 
   else {
      /* outside */
      nratio = 1.0 / nratio;
      setcolor(ctrans, 
               -mylog(mat -> oargs.farg [0] * rayin -> pcol [0]),
               -mylog(mat -> oargs.farg [1] * rayin -> pcol [1]),
               -mylog(mat -> oargs.farg [2] * rayin -> pcol [2]));
      setcolor(talb, 0, 0, 0);
      if (mat -> otype == MAT_INTERFACE) {
         setcolor(rayin -> cext,
                  -mylog(mat -> oargs.farg [4] * rayin -> pcol [0]),
                  -mylog(mat -> oargs.farg [5] * rayin -> pcol [1]),
                  -mylog(mat -> oargs.farg [6] * rayin -> pcol [2]));
         setcolor(rayin -> albedo, 0, 0, 0);
         rayin -> gecc = 0;
      }
   }            
   /* compute cos theta2 */
   d2 = 1 - nratio * nratio * (1 - cos1 * cos1);
   if (d2 < FTINY) 
      /* total reflection */	
      refl = 1;
   else {
      /* refraction occurs; compute Fresnel's equations */
      cos2 = sqrt(d2);
      d1 = cos1;
      d2 = nratio * cos2;
      d1 = (d1 - d2) / (d1 + d2);
      refl = d1 * d1;
      d1 = 1 / cos1;
      d2 = nratio / cos2;
      d1 = (d1 - d2) / (d1 + d2);
      refl += d1 * d1;
      refl *= 0.5;
      trans = 1 - refl;
   }
   if (pmapRandom(rouletteState) > refl) {
      /* Refraction */
      rayorigin(&rayout, REFRACTED | SPECULAR, rayin, NULL);
      copycolor(rayout.rcol, rayin -> rcol);
      d1 = nratio * cos1 - cos2;
      for (i = 0; i < 3; i++)
         rayout.rdir [i] = nratio * rayin -> rdir [i] + d1 * dnorm [i];
      if (hastexture && 
          DOT(rayout.rdir, rayin -> ron) * hastexture >= -FTINY) {
         d1 *= hastexture;
         for (i = 0; i < 3; i++)
            rayout.rdir [i] = nratio * rayin -> rdir [i] + 
                              d1 * rayin -> ron [i];
         normalize(rayout.rdir);
      }
      copycolor(rayout.cext, ctrans);
      copycolor(rayout.albedo, talb);
   }
   else {
      /* Reflection */
      rayorigin(&rayout, REFLECTED | SPECULAR, rayin, NULL);
      copycolor(rayout.rcol, rayin -> rcol);                              
      VSUM(rayout.rdir, rayin -> rdir, dnorm, 2 * cos1);
      if (hastexture && 
          DOT(rayout.rdir, rayin -> ron) * hastexture <= FTINY)
         for (i = 0; i < 3; i++)
            rayout.rdir [i] = rayin -> rdir [i] + 
                              2 * rayin -> rod * rayin -> ron [i];
   }
   /* Set specular flag */
   rayout.rsrc = 1;
   /* Ray is modified by medium in photonParticipate() */
   tracePhoton(&rayout);
}



static int glassPhotonScatter (OBJREC* mat, RAY* rayIn)
/* Generate new photon ray for glass material and recurse */
{
   float albedo, xi, ptrans;
   COLOR mcolor, refl, trans;
   double pdot, cos2, d, r1e, r1m;
   double rindex= 0.0;
   FVECT pnorm, pdir;
   register int i, hastexture;
   RAY rayOut;

   /* check arguments */
   if (mat -> oargs.nfargs == 3)
	   rindex = RINDEX;
   else if (mat -> oargs.nfargs == 4)
	   rindex = mat -> oargs.farg [3];
   else
	   objerror(mat, USER, "bad arguments");

   copycolor(mcolor, mat -> oargs.farg);   
   /* get modifiers */
   raytexture(rayIn, mat -> omod);
   /* reorient if necessary */
   if (rayIn -> rod < 0) flipsurface(rayIn);
   if ( (hastexture = DOT(rayIn -> pert, rayIn -> pert)) > FTINY * FTINY)
      pdot = raynormal(pnorm, rayIn);
   else {
      VCOPY(pnorm, rayIn -> ron);
      pdot = rayIn -> rod;
   }
   /* Modify material color */
   multcolor(mcolor, rayIn -> pcol);
   /* angular transmission */
   cos2 = sqrt((1 - 1 / (rindex * rindex)) + 
               pdot * pdot / (rindex * rindex));
   setcolor(mcolor, pow(mcolor [0], 1 / cos2),
            pow(mcolor [1], 1 / cos2), pow(mcolor [2], 1 / cos2));
   /* compute reflection */
   r1e = (pdot - rindex * cos2) / (pdot + rindex * cos2);
   r1e *= r1e;
   r1m = (1 / pdot - rindex / cos2) / (1 / pdot + rindex / cos2);
   r1m *= r1m;
   for (i = 0; i < 3; i++) {
      d = mcolor [i];
      /* compute transmittance */
      trans [i] = 0.5 * d * 
                  ((1 - r1e) * (1 - r1e) / (1 - r1e * r1e * d * d) +
                   (1 - r1m) * (1 - r1m) / (1 - r1m * r1m * d * d));
      /* compute reflectance */
      d *= d;
      refl [i] = 0.5 * 
                 (r1e * (1 + (1 - 2 * r1e) * d) / (1 - r1e * r1e * d) +
                  r1m * (1 + (1 - 2 * r1m) * d) / (1 - r1m * r1m * d));
   }
   /* Set up probabilities */
   ptrans = colorAvg(trans);
   albedo = colorAvg(refl) + ptrans;
   xi = pmapRandom(rouletteState);
   /* Absorbed? */
   if (xi > albedo) return;
   if (xi > (albedo -= ptrans)) {
      /* transmitted ray */
      if (hastexture) {
         /* perturb direction */
         VSUM(pdir, rayIn -> rdir, rayIn -> pert, 2 * (1 - rindex));
         if (normalize(pdir) == 0) {
            objerror(mat, WARNING, "bad perturbation");
            VCOPY(pdir, rayIn -> rdir);
         }
      } 
      else VCOPY(pdir, rayIn -> rdir);
      VCOPY(rayOut.rdir, pdir);
      rayorigin(&rayOut, TRANS | SPECULAR, rayIn, NULL);      
   }
   else {
      /* reflected ray */
      VSUM(rayOut.rdir, rayIn -> rdir, pnorm, 2 * pdot);
      rayorigin(&rayOut, REFLECTED | SPECULAR, rayIn, NULL);      
   }
   /* Modify ray colour and normalise */
   copycolor(rayOut.rcol, rayIn -> rcol);
   multcolor(rayOut.rcol, mcolor);
   colorNorm(rayOut.rcol);
   /* Set specular flag */
   rayOut.rsrc = 1;
   tracePhoton(&rayOut);
}



static int aliasPhotonScatter (OBJREC* mat, RAY *rayIn)
/* Transfer photon scattering to alias target */
{
   OBJECT aliasObj;
   OBJREC aliasRec;
   
   /* Straight replacement? */
   if (!mat -> oargs.nsargs) {
      mat = objptr(mat -> omod);
      photonScatter [mat -> otype] (mat, rayIn);
      return;
   }
   /* Else replace alias */
   if (mat -> oargs.nsargs != 1)
      objerror(mat, INTERNAL, "bad # string arguments");
   aliasObj = lastmod(objndx(mat), mat -> oargs.sarg [0]);
   if (aliasObj < 0) objerror(mat, USER, "bad reference");
   memcpy(&aliasRec, objptr(aliasObj), sizeof(OBJREC));
   /* Substitute modifier */
   aliasRec.omod = mat -> omod;
   /* Replacement scattering routine */
   photonScatter [aliasRec.otype] (&aliasRec, rayIn);
}



static int clipPhotonScatter (OBJREC* mat, RAY* rayin)
/* Generate new photon ray for antimatter material and recurse */
{
   OBJECT obj, mod, cset [MAXSET + 1], *modset;
   int entering, inside = 0;
   register int i;
   register const RAY *rp;
   RAY rayout;

   obj = objndx(mat);
   if ((modset = (OBJECT*)mat -> os) == NULL) {      
      if (mat -> oargs.nsargs < 1 || mat -> oargs.nsargs > MAXSET)
         objerror(mat, USER, "bad # arguments");
      modset = (OBJECT*)malloc((mat -> oargs.nsargs + 1) * sizeof(OBJECT));
      if (modset == NULL) error(SYSTEM, "out of memory in m_clip");
      modset [0] = 0;
      for (i = 0; i < mat -> oargs.nsargs; i++) {
         if (!strcmp(mat -> oargs.sarg [i], VOIDID)) continue;
         if ((mod = lastmod(obj, mat -> oargs.sarg [i])) == OVOID) {
            sprintf(errmsg, "unknown modifier \"%s\"", 	
                    mat -> oargs.sarg [i]);
            objerror(mat, WARNING, errmsg);
            continue;
         }
         if (inset(modset, mod)) {
            objerror(mat, WARNING, "duplicate modifier");
            continue;
         }
         insertelem(modset, mod);
      }
      mat -> os = (char*)modset;
   }
   if (rayin -> clipset != NULL) setcopy(cset, rayin -> clipset);
   else cset [0] = 0;
   entering = rayin -> rod > 0;
   for (i = modset [0]; i > 0; i--) {
      if (entering) {
         if (!inset(cset, modset [i])) {
            if (cset [0] >= MAXSET) 
               error(INTERNAL, "set overflow in m_clip");
            insertelem(cset, modset [i]);
         }
      } 
      else if (inset(cset, modset [i])) deletelem(cset, modset [i]);
   }
   rayin -> newcset = cset;
   if (strcmp(mat -> oargs.sarg [0], VOIDID)) {
	   for (rp = rayin; rp -> parent != NULL; rp = rp -> parent) {
		   if ( !(rp -> rtype & RAYREFL) && rp->parent->ro != NULL && 
				inset(modset, rp -> parent -> ro -> omod)) {
			   if (rp -> parent -> rod > 0)
				   inside++;
			   else
				   inside--;
		   }
	   }
	   if (inside > 0) {
		   flipsurface(rayin);
		   mat = objptr(lastmod(obj, mat -> oargs.sarg [0]));
		   photonScatter [mat -> otype] (mat, rayin);
		   return;
	   }
   }
   /* else transfer ray */
   rayorigin(&rayout, TRANS, rayin, NULL);
   VCOPY(rayout.rdir, rayin -> rdir);
   copycolor(rayout.rcol, rayin -> rcol);
   rayout.rsrc = rayin -> rsrc;
   tracePhoton(&rayout);
}



static int mirrorPhotonScatter (OBJREC* mat, RAY* rayIn)
/* Generate new photon ray for mirror material and recurse */
{
   RAY rayOut;
   int rpure = 1;
   register int i;
   FVECT pnorm;
   double pdot;
   float albedo;
   COLOR mcolor;

   /* check arguments */
   if (mat -> oargs.nfargs != 3 || mat -> oargs.nsargs > 1)
      objerror(mat, USER, "bad number of arguments");
   /* back is black */
   if (rayIn -> rod < 0) return;
   /* get modifiers */
   raytexture(rayIn, mat -> omod);
   /* assign material color */
   copycolor(mcolor, mat -> oargs.farg); 
   multcolor(mcolor, rayIn -> pcol);
   /* Set up probabilities */
   albedo = colorAvg(mcolor);
   /* Absorbed? */
   if (pmapRandom(rouletteState) > albedo) return;
   /* compute reflected ray */
   rayorigin(&rayOut, REFLECTED | SPECULAR, rayIn, NULL);
   /* Modify ray colour and normalise */
   copycolor(rayOut.rcol, rayIn -> rcol);   
   multcolor(rayOut.rcol, mcolor);
   colorNorm(rayOut.rcol);    
   /* Set specular flag */
   rayOut.rsrc = 1;
   if (DOT(rayIn -> pert, rayIn -> pert) > FTINY * FTINY) {
      /* use textures */
      pdot = raynormal(pnorm, rayIn);   
      for (i = 0; i < 3; i++)
         rayOut.rdir [i] = rayIn -> rdir [i] + 2 * pdot * pnorm [i];
      rpure = 0;
   }
   /* check for penetration (X-rated code starts here) */
   if (rpure || DOT(rayOut.rdir, rayIn -> ron) <= FTINY)
      for (i = 0; i < 3; i++)
         rayOut.rdir [i] = rayIn -> rdir [i] + 
                           2 * rayIn -> rod * rayIn -> ron [i];
   tracePhoton(&rayOut);
}



static int mistPhotonScatter (OBJREC* mat, RAY* rayin)
/* Generate new photon ray within mist and recurse */
{
   COLOR mext;
   RREAL re, ge, be;
   RAY rayout;

   /* check arguments */
   if (mat -> oargs.nfargs > 7) objerror(mat, USER, "bad arguments");
   if (mat -> oargs.nfargs > 2) {
      /* compute extinction */
      copycolor(mext, mat -> oargs.farg);
      /* get modifiers */
      raytexture(rayin, mat -> omod);
      multcolor(mext, rayin -> pcol);
   } 
   else setcolor(mext, 0, 0, 0);
   rayorigin(&rayout, TRANS, rayin, NULL);
   VCOPY(rayout.rdir, rayin -> rdir);
   copycolor(rayout.rcol, rayin -> rcol);
   if (rayin -> rod > 0) {
      /* entering ray */
      addcolor(rayout.cext, mext);
      if (mat -> oargs.nfargs > 5) 
         copycolor(rayout.albedo, mat -> oargs.farg + 3);
      if (mat -> oargs.nfargs > 6) rayout.gecc = mat -> oargs.farg [6];
   } 
   else {
      /* leaving ray */
      if ((re = rayin -> cext [0] - mext [0]) < cextinction [0])
         re = cextinction [0];
      if ((ge = rayin -> cext [1] - mext [1]) < cextinction [1])
         ge = cextinction [1];
      if ((be = rayin -> cext [2] - mext [2]) < cextinction [2])
         be = cextinction [2];
      setcolor(rayout.cext, re, ge, be);
      if (mat -> oargs.nfargs > 5) copycolor(rayout.albedo, salbedo);
      if (mat -> oargs.nfargs > 6) rayout.gecc = seccg;
   }
   /* Reset specular flag??? */
   /* rayout.rsrc = 0; */
   rayout.rsrc = rayin -> rsrc;
   tracePhoton(&rayout);
}



static int mx_dataPhotonScatter (OBJREC* mat, RAY* rayin)
/* Pass photon on to materials selected by mixture data */
{
   OBJECT obj;
   double coef, pt [MAXDIM];
   DATARRAY *dp;
   OBJECT mod [2];
   register MFUNC *mf;
   register int i;

   if (mat -> oargs.nsargs < 6) objerror(mat, USER, "bad # arguments");
   obj = objndx(mat);
   for (i = 0; i < 2; i++)
      if (!strcmp(mat -> oargs.sarg [i], VOIDID)) mod [i] = OVOID;
      else if ((mod [i] = lastmod(obj, mat -> oargs.sarg [i])) == OVOID) {
         sprintf(errmsg, "undefined modifier \"%s\"", 
                 mat -> oargs.sarg [i]);
         objerror(mat, USER, errmsg);
      }
   dp = getdata(mat -> oargs.sarg [3]);
   i = (1 << dp -> nd) - 1;
   mf = getfunc(mat, 4, i << 5, 0);
   setfunc(mat, rayin);
   errno = 0;
   for (i = 0; i < dp -> nd; i++) {
      pt [i] = evalue(mf -> ep [i]);
      if (errno) {
         objerror(mat, WARNING, "compute error");
         return;
      }
   }
   coef = datavalue(dp, pt);
   errno = 0;
   coef = funvalue(mat -> oargs.sarg [2], 1, &coef);
   if (errno) objerror(mat, WARNING, "compute error");
   else {
      mat = objptr(mod [pmapRandom(rouletteState) < coef ? 0 : 1]);
      photonScatter [mat -> otype] (mat, rayin);
   }
}



static int mx_pdataPhotonScatter (OBJREC* mat, RAY* rayin)
/* Pass photon on to materials selected by mixture picture */
{
   OBJECT obj;
   double col [3], coef, pt [MAXDIM];
   DATARRAY *dp;
   OBJECT mod [2];
   register MFUNC *mf;
   register int i;

   if (mat -> oargs.nsargs < 7) objerror(mat, USER, "bad # arguments");
   obj = objndx(mat);
   for (i = 0; i < 2; i++)
      if (!strcmp(mat -> oargs.sarg [i], VOIDID)) mod [i] = OVOID;
      else if ((mod [i] = lastmod(obj, mat -> oargs.sarg [i])) == OVOID) {
         sprintf(errmsg, "undefined modifier \"%s\"", 
                 mat -> oargs.sarg [i]);
         objerror(mat, USER, errmsg);
      }
   dp = getpict(mat -> oargs.sarg [3]);
   mf = getfunc(mat, 4, 0x3<<5, 0);
   setfunc(mat, rayin);
   errno = 0;
   pt [1] = evalue(mf -> ep [0]);
   pt [0] = evalue(mf -> ep [1]);
   if (errno) {
      objerror(mat, WARNING, "compute error");
      return;
   }
   for (i = 0; i < 3; i++) col [i] = datavalue(dp + i, pt);
   errno = 0;
   coef = funvalue(mat -> oargs.sarg [2], 3, col);
   if (errno) objerror(mat, WARNING, "compute error");
   else {
      mat = objptr(mod [pmapRandom(rouletteState) < coef ? 0 : 1]);
      photonScatter [mat -> otype] (mat, rayin);
   }   
}



static int mx_funcPhotonScatter (OBJREC* mat, RAY* rayin)
/* Pass photon on to materials selected by mixture function */
{
   OBJECT obj, mod [2];
   register int i;
   double coef;
   register MFUNC *mf;

   if (mat -> oargs.nsargs < 4) objerror(mat, USER, "bad # arguments");
   obj = objndx(mat);
   for (i = 0; i < 2; i++)
      if (!strcmp(mat -> oargs.sarg [i], VOIDID))
         mod [i] = OVOID;
      else if ((mod [i] = lastmod(obj, mat -> oargs.sarg [i])) 
               == OVOID) {
         sprintf(errmsg, "undefined modifier \"%s\"",
                 mat -> oargs.sarg [i]);
         objerror(mat, USER, errmsg);
      }
   mf = getfunc(mat, 3, 0x4, 0);
   setfunc(mat, rayin);
   errno = 0;
   coef = evalue(mf -> ep [0]);
   if (errno) objerror(mat, WARNING, "compute error");
   else {
      /* bound coefficient */
      if (coef > 1) coef = 1;
      else if (coef < 0) coef = 0;
      mat = objptr(mod [pmapRandom(rouletteState) < coef ? 0 : 1]);
      photonScatter [mat -> otype] (mat, rayin);
   }
}



static int pattexPhotonScatter (OBJREC* mat, RAY* rayIn)
/* Generate new photon ray for pattern or texture modifier and recurse.
   This code is brought to you by Henkel! :^) */
{
   RAY rayOut;
   
   /* Get pattern */
   ofun [mat -> otype].funp(mat, rayIn);
   if (mat -> omod != OVOID) {
      /* Scatter using modifier (if any) */
      mat = objptr(mat -> omod);
      photonScatter [mat -> otype] (mat, rayIn);
   }
   else {
      /* Transfer ray if no modifier */
      rayorigin(&rayOut, TRANS, rayIn, NULL);
      VCOPY(rayOut.rdir, rayIn -> rdir);
      copycolor(rayOut.rcol, rayIn -> rcol);
      rayOut.rsrc = rayIn -> rsrc;
      tracePhoton(&rayOut);
   }
}



static int lightPhotonScatter (OBJREC* mat, RAY* ray)
/* Light sources absorb photons. */
{
   return 0;
}



void initPhotonScatterFuncs ()
/* Init photonScatter[] dispatch table */
{
   register int i;
   
   for (i = 0; i < NUMOTYPE; i++) photonScatter [i] = o_default;
   photonScatter [MAT_LIGHT] = photonScatter [MAT_ILLUM] =
      photonScatter [MAT_GLOW] = photonScatter [MAT_SPOT] = 
         lightPhotonScatter;
   photonScatter [MAT_PLASTIC] = photonScatter [MAT_METAL] =
      photonScatter [MAT_TRANS] = normalPhotonScatter;
   photonScatter [MAT_PLASTIC2] = photonScatter [MAT_METAL2] =
      photonScatter [MAT_TRANS2] = anisoPhotonScatter;
   photonScatter [MAT_DIELECTRIC] = photonScatter [MAT_INTERFACE] = 
      dielectricPhotonScatter;
   photonScatter [MAT_MIST] = mistPhotonScatter;
   photonScatter [MAT_GLASS] = glassPhotonScatter;
   photonScatter [MAT_CLIP] = clipPhotonScatter;
   photonScatter [MAT_MIRROR] = mirrorPhotonScatter;
   photonScatter [MIX_FUNC] = mx_funcPhotonScatter;
   photonScatter [MIX_DATA] = mx_dataPhotonScatter;
   photonScatter [MIX_PICT]= mx_pdataPhotonScatter;
   photonScatter [PAT_BDATA] = photonScatter [PAT_CDATA] =
      photonScatter [PAT_BFUNC] = photonScatter [PAT_CFUNC] =
         photonScatter [PAT_CPICT] = photonScatter [TEX_FUNC] = 
            photonScatter [TEX_DATA] = pattexPhotonScatter;
   photonScatter [MOD_ALIAS] = aliasPhotonScatter;
}
