#ifndef PMAPPARAMS_H
#define PMAPPARAMS_H

/* 
   ==================================================================
   Parameters common to all photon map modules. 
   For inclusion in main program.
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



/* Default minimum irradiance from photon density estimates */
#define PMAP_IRRAD_THRESH 0.01



/* Struct for passing params per photon map */
typedef struct {
   char *fileName;                /* Photon map file */
   unsigned minGather, maxGather; /* Num photons to gather */
   unsigned long distribTarget;   /* Num photons to store */
   float irradThresh;             /* Min. irrad. from density estimates */
} PhotonMapParams;



extern PhotonMapParams globalPmapParams, causticPmapParams, 
                       volumePmapParams, directPmapParams;
extern float pdfSamples, preDistrib, finalGather; 
extern unsigned long photonHeapSizeInc, photonMaxBounce;
extern unsigned photonRepTime, maxPreDistrib;

#endif
