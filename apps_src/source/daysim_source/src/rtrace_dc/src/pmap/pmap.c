/* 
   ==================================================================
   Photon map main module
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmap.h"
#include "pmapmat.h"
#include "pmapsrc.h"
#include "pmaprand.h"
#include "pmapio.h"
#include "pmapmisc.h"
#include "pmapbias.h"
#include "otypes.h"
#include <signal.h>
#include <time.h>
#include <sys/stat.h>

#include "daysim.h" // always include header

extern char *octname;
extern OBJECT ambset [];



static char header [] = "$Revision: 4.3 $";



SRCREC *photonPorts = NULL;             /* Photon ports */
unsigned numPhotonPorts = 0;



void setPmapParams (PhotonMap** gpm, const PhotonMapParams* gpmParm,
                    PhotonMap** cpm, const PhotonMapParams* cpmParm,
                    PhotonMap** vpm, const PhotonMapParams* vpmParm,
                    PhotonMap** dpm, const PhotonMapParams* dpmParm)
{
   if (gpmParm -> fileName) {
      *gpm = (PhotonMap*)malloc(sizeof(PhotonMap));
      (*gpm) -> fileName = gpmParm -> fileName;
      (*gpm) -> minGather = gpmParm -> minGather;
      (*gpm) -> maxGather = gpmParm -> maxGather;
      (*gpm) -> distribTarget = gpmParm -> distribTarget;
      (*gpm) -> maxInitDist = gpmParm -> irradThresh;
      (*gpm) -> heapSizeInc = photonHeapSizeInc;
   }
   if (cpmParm -> fileName) {
      *cpm = (PhotonMap*)malloc(sizeof(PhotonMap));
      (*cpm) -> fileName = cpmParm -> fileName;
      (*cpm) -> minGather = cpmParm -> minGather;
      (*cpm) -> maxGather = cpmParm -> maxGather;
      (*cpm) -> distribTarget = cpmParm -> distribTarget;
      (*cpm) -> maxInitDist = cpmParm -> irradThresh;
      (*cpm) -> heapSizeInc = photonHeapSizeInc;
   }
   if (vpmParm -> fileName) {
      *vpm = (PhotonMap*)malloc(sizeof(PhotonMap));
      (*vpm) -> fileName = vpmParm -> fileName;
      (*vpm) -> minGather = vpmParm -> minGather;
      (*vpm) -> maxGather = vpmParm -> maxGather;
      (*vpm) -> distribTarget = vpmParm -> distribTarget;
      (*vpm) -> maxInitDist = vpmParm -> irradThresh;
      (*vpm) -> heapSizeInc = photonHeapSizeInc;
   }
   if (dpmParm -> fileName) {
      *dpm = (PhotonMap*)malloc(sizeof(PhotonMap));
      (*dpm) -> fileName = dpmParm -> fileName;
      (*dpm) -> minGather = dpmParm -> minGather;
      (*dpm) -> maxGather = dpmParm -> maxGather;
      (*dpm) -> distribTarget = dpmParm -> distribTarget;
      (*dpm) -> maxInitDist = dpmParm -> irradThresh;
      (*dpm) -> heapSizeInc = photonHeapSizeInc;                      
   }
}



void loadPmaps (PhotonMap* gpm, PhotonMap* cpm, PhotonMap* vpm, 
                PhotonMap* dpm)
{
   struct stat octstat, pmstat;
   
   if (gpm) {
      if (!stat(gpm -> fileName, &pmstat) &&
          !stat(octname, &octstat) && octstat.st_mtime > pmstat.st_mtime)
         error(WARNING, "global photon map may be stale");
      loadPhotonMap(gpm, gpm -> fileName, NULL, 
                    gpm -> maxGather ? GLOBAL_PMAP_MAGIC 
                                     : PRECOMP_GLOBAL_PMAP_MAGIC);
      if (gpm -> maxGather > gpm -> heapSize) {
         error(WARNING, "adjusting global density estimate bandwidth");
         gpm -> minGather = gpm -> maxGather = gpm -> heapSize;
      }
   }
   if (cpm) {
      if (!stat(cpm -> fileName, &pmstat) &&
          !stat(octname, &octstat) && octstat.st_mtime > pmstat.st_mtime)
         error(WARNING, "caustic photon map may be stale");
      loadPhotonMap(cpm, cpm -> fileName, NULL, CAUSTIC_PMAP_MAGIC);
      if (cpm -> maxGather > cpm -> heapSize) {
         error(WARNING, "adjusting caustic density estimate bandwidth");
         cpm -> minGather = cpm -> maxGather = cpm -> heapSize;
      }
   }
   if (vpm) {
      if (!stat(vpm -> fileName, &pmstat) &&
          !stat(octname, &octstat) && octstat.st_mtime > pmstat.st_mtime)
         error(WARNING, "volume photon map may be stale");
      loadPhotonMap(vpm, vpm -> fileName, NULL, VOLUME_PMAP_MAGIC);
      if (vpm -> maxGather > vpm -> heapSize) {
         error(WARNING, "adjusting volume density estimate bandwidth");
         vpm -> minGather = vpm -> maxGather = vpm -> heapSize;
      }
   }
   if (dpm) {
      if (!stat(dpm -> fileName, &pmstat) &&
          !stat(octname, &octstat) && octstat.st_mtime > pmstat.st_mtime)
         error(WARNING, "direct photon map may be stale");
      loadPhotonMap(dpm, dpm -> fileName, NULL, DIRECT_PMAP_MAGIC);
      if (dpm -> maxGather > dpm -> heapSize) {
         error(WARNING, "adjusting direct density estimate bandwidth");
         dpm -> minGather = dpm -> maxGather = dpm -> heapSize;
      }   
   }
}



void savePmaps (PhotonMap* gpm, PhotonMap* cpm, PhotonMap* vpm, 
                PhotonMap* dpm, char* cmd)
{
   if (gpm) savePhotonMap(gpm, gpm -> fileName, cmd, 
                          gpm -> maxGather ? PRECOMP_GLOBAL_PMAP_MAGIC
                                           : GLOBAL_PMAP_MAGIC);
   if (cpm) savePhotonMap(cpm, cpm -> fileName, cmd, CAUSTIC_PMAP_MAGIC);
   if (vpm) savePhotonMap(vpm, vpm -> fileName, cmd, VOLUME_PMAP_MAGIC);
   if (dpm) savePhotonMap(dpm, dpm -> fileName, cmd, DIRECT_PMAP_MAGIC);
}


   
void cleanUpPmaps (PhotonMap* gpm, PhotonMap* cpm, PhotonMap* vpm, 
                   PhotonMap* dpm)
{
   if (gpm) {
      deletePhotons(gpm);
      free(gpm);
   }
   if (cpm) {
      deletePhotons(cpm);
      free(cpm);
   }
   if (vpm) {
      deletePhotons(vpm);
      free(vpm);
   }
   if (dpm) {
      deletePhotons(dpm);
      free(dpm);
   }
}


     
static int photonParticipate (RAY* ray)
/* Trace photon through participating medium. Yeah man, that 
   stuff that really participates. Returns 1 if passed through, 
   or 0 if absorbed and $*%&ed. Analogon to rayparticipate(). */
{
   register int i;
   RREAL cosTheta, cosPhi, du, dv;
   const float cext = colorAvg(ray -> cext),
               albedo = colorAvg(ray -> albedo);
   FVECT u, v;
   COLOR cvext;

   /* Mean free distance until interaction with medium */
   ray -> rmax = -log(pmapRandom(mediumState)) / cext;
   while (!localhit(ray, &thescene)) {
      setcolor(cvext, exp(-ray -> rmax * ray -> cext [0]),
                      exp(-ray -> rmax * ray -> cext [1]),
                      exp(-ray -> rmax * ray -> cext [2]));
      /* Modify ray color and normalise */
      multcolor(ray -> rcol, cvext);
      colorNorm(ray -> rcol);
      VCOPY(ray -> rorg, ray -> rop);
      if (albedo > FTINY)
         /* Add to volume photon map */
         if (ray -> rlvl > 0) addPhoton(volumePmap, ray);
      /* Absorbed? */
      if (pmapRandom(rouletteState) > albedo) return 0;
      /* Colour bleeding without attenuation */
      multcolor(ray -> rcol, ray -> albedo);
      scalecolor(ray -> rcol, 1 / albedo);    
      /* Scatter photon */
      cosTheta = ray -> gecc <= FTINY 
                 ? 2 * pmapRandom(scatterState) - 1
                 : 1 / (2 * ray -> gecc) * 
                   (1 + ray -> gecc * ray -> gecc - 
                    (1 - ray -> gecc * ray -> gecc) / 
                    (1 - ray -> gecc + 2 * ray -> gecc *
                    pmapRandom(scatterState)));
      cosPhi = cos(2 * PI * pmapRandom(scatterState));
      du = dv = sqrt(1 - cosTheta * cosTheta);   /* sin(theta) */
      du *= cosPhi;
      dv *= sqrt(1 - cosPhi * cosPhi);           /* sin(phi) */
      /* Get axes u & v perpendicular to photon direction */
      i = 0;
      do {
         v [0] = v [1] = v [2] = 0;
         v [i++] = 1;
         fcross(u, v, ray -> rdir);
      } while (normalize(u) < FTINY);
      fcross(v, ray -> rdir, u);
      for (i = 0; i < 3; i++)
         ray -> rdir [i] = du * u [i] + dv * v [i] + 
                           cosTheta * ray -> rdir [i];
      ray -> rlvl++;
      ray -> rmax = -log(pmapRandom(mediumState)) / cext;
   }   
   setcolor(cvext, exp(-ray -> rot * ray -> cext [0]),
                   exp(-ray -> rot * ray -> cext [1]),
                   exp(-ray -> rot * ray -> cext [2]));
   /* Modify ray color and normalise */
   multcolor(ray -> rcol, cvext);
   colorNorm(ray -> rcol);
   /* Passed through medium */  
   return 1;
}



void tracePhoton (RAY* ray)
/* Follow photon as it bounces around... */
{
   register int mod;
   OBJREC* mat;
 
   if (ray -> rlvl > photonMaxBounce) {
      error(WARNING, "runaway photon!");
      return;
   }
   if (colorAvg(ray -> cext) > FTINY && !photonParticipate(ray)) return;
   if (localhit(ray, &thescene)) {
      mod = ray -> ro -> omod;
      if (ray -> clipset && inset(ray -> clipset, mod)) {
         /* Transfer ray for antimattah... */
         RAY tray;
         rayorigin(&tray, TRANS, ray, NULL);
         VCOPY(tray.rdir, ray -> rdir);
         copycolor(tray.rcol, ray -> rcol);
         tracePhoton(&tray);
      }
      else {
         mat = objptr(mod);
         photonScatter [mat -> otype] (mat, ray);
      }
   }
}



static void getPorts ()
/* Find geometry declared as photon ports */
{
   register OBJECT i;
   OBJREC* obj;
   
   for (i = 0; i < nobjects; i++) {
      obj = objptr(i);
      if (inset(ambset, obj -> omod)) {
         /* Add photon port */
         photonPorts = (SRCREC*)realloc(photonPorts, 
                                        (numPhotonPorts + 1) * 
                                        sizeof(SRCREC));
         if (!photonPorts) error(USER, "can't allocate photon ports");
         photonPorts [numPhotonPorts].so = obj;
         photonPorts [numPhotonPorts].sflags = 0;
         if (!sfun [obj -> otype].of || !sfun[obj -> otype].of -> setsrc)
            objerror(obj, USER, "illegal photon port");
         setsource(photonPorts + numPhotonPorts, obj);
         numPhotonPorts++;
      }
   }
}



static void preComputeGlobal (PhotonMap* pmap)
/* Precompute irradiance from global photons for final gathering using 
   the first finalGather * pmap -> heapSize photons in the heap. Returns 
   new heap with precomputed photons. */
{
   register unsigned long i, nuHeapSize;
   register unsigned j;
   Photon *nuHeap, *p;
   COLOR irrad;
   RAY ray;
   float nuMinPos [3], nuMaxPos [3];

   repComplete = nuHeapSize = finalGather * pmap -> heapSize;
   if (photonRepTime) {
      sprintf(errmsg, 
              "Precomputing irradiance for %ld global photons...\n", 
              nuHeapSize);
      eputs(errmsg);
      fflush(stderr);
   }
   p = nuHeap = (Photon*)malloc(nuHeapSize * sizeof(Photon));
   if (!nuHeap) error(USER, "can't allocate photon heap");
   for (j = 0; j <= 2; j++) {
      nuMinPos [j] = FHUGE;
      nuMaxPos [j] = -FHUGE;
   }
   /* record start time */
   repStartTime = time(NULL);
   signal(SIGCONT, preComputeReport);
   repProgress = 0;
   bcopy(pmap -> heap, nuHeap, nuHeapSize * sizeof(Photon));
   for (i = 0, p = nuHeap; i < nuHeapSize; i++, p++) {
      ray.ro = NULL;
      VCOPY(ray.rop, p -> pos);
      /* Update min and max positions & set ray normal */
      for (j = 0; j < 3; j++) {
         if (p -> pos [j] < nuMinPos [j]) nuMinPos [j] = p -> pos [j];
         if (p -> pos [j] > nuMaxPos [j]) nuMaxPos [j] = p -> pos [j];
         ray.ron [j] = p -> norm [j] / 127.0;
      }
#ifndef DAYSIM
      photonDensity(pmap, &ray, irrad);
#endif
      setPhotonFlux(p, irrad);
      repProgress++;
      if (photonRepTime > 0 && time(NULL) >= repLastTime + photonRepTime)
         preComputeReport();
#ifndef BSD
      else signal(SIGCONT, preComputeReport);
#endif
   }
   signal(SIGCONT, SIG_DFL);
   /* Replace & rebuild heap */
   free(pmap -> heap);
   pmap -> heap = nuHeap;
   pmap -> heapSize = pmap -> heapEnd = nuHeapSize;
   VCOPY(pmap -> minPos, nuMinPos);
   VCOPY(pmap -> maxPos, nuMaxPos);
   if (photonRepTime) {
      eputs("Rebuilding global photon heap...\n");
      fflush(stderr);
   }
   balancePhotons(pmap, 1);
}



void distribPhotons (PhotonMap* gpm, PhotonMap* cpm, PhotonMap* vpm, 
                     PhotonMap* dpm)
{
   static RAY photonRay;
   EmissionMap emap;
   register unsigned srcnum, portCnt, pass = 0;
   register long emitCnt;
   double totalFlux = 0;
   float numEmit, ec;
   char errmsg2 [128];

   emap.samples = NULL;
   emap.maxPartitions = MAXSPART;
   if (!(emap.partitions = (unsigned char*)malloc(emap.maxPartitions >> 1)))
      error(USER, "can't allocate source partitions");
   initPhotonMap(gpm);
   initPhotonMap(cpm);
   initPhotonMap(vpm);
   /* Set up emission and scattering funcs */
   initPhotonEmissionFuncs();
   initPhotonScatterFuncs();
   /* Get photon ports if specified */
   if (ambincl == 1) getPorts();
   /* Seed RNGs for photon distribution */
   pmapSeed(45707, partState);
   pmapSeed(31771, emitState);
   pmapSeed(12323, cntState);
   pmapSeed(39887, mediumState);
   pmapSeed(7541, scatterState);
   pmapSeed(51673, rouletteState);
   /* Get total photon flux */
   for (srcnum = 0; srcnum < nsources; srcnum++) {         
      emap.src = source + srcnum;
      portCnt = 0;      
      do {
         emap.port = emap.src -> sflags & SDISTANT 
                     ? photonPorts + portCnt : NULL;
         photonPartition [emap.src -> so -> otype] (&emap);
         if (photonRepTime) {
            sprintf(errmsg, "Integrating flux from source %s ", 
                    source [srcnum].so -> oname);
            if (emap.port) {
               sprintf(errmsg2, "via port %s ", 
                       photonPorts [portCnt].so -> oname);
               strcat(errmsg, errmsg2);
            }
            sprintf(errmsg2, "(%lu partitions)...\n", emap.numPartitions);
            strcat(errmsg, errmsg2);
            eputs(errmsg);
            fflush(stderr);
         }
         for (emap.partitionCnt = 0; emap.partitionCnt < emap.numPartitions;
              emap.partitionCnt++) {
            sourceFlux(&emap);
            /* Validation shows that we need a double here */
            totalFlux += colorAvg(emap.partFlux);
         }
         portCnt++;
      } while (portCnt < numPhotonPorts);
   }
   if (totalFlux < FTINY) error(USER, "zero emission!");
   /* record start time */
   repStartTime = time(NULL);
   signal(SIGCONT, distribReport);
   repProgress = 0;
   /* numEmit for prepass is minimum target count */
   numEmit = min(gpm ? gpm -> distribTarget : FHUGE,
                 min(cpm ? cpm -> distribTarget : FHUGE,
                     min(vpm ? vpm -> distribTarget : FHUGE,
                         dpm ? dpm -> distribTarget : FHUGE)));
   /* Distribution ratios have no effect in prepass */
   if (gpm) gpm -> distribRatio = 1;
   if (cpm) cpm -> distribRatio = 1;
   if (vpm) vpm -> distribRatio = 1;
   if (dpm) dpm -> distribRatio = 1;
   /* Do prepass emitting preDistrib * numEmit */ 
   do {
      /* Terminate if we're stuck */
      if (++pass > maxPreDistrib)
         error(USER, "too many prepasses, photon map(s) empty.");
      /* Set completion count for progress report */
      repComplete = preDistrib * numEmit + repProgress;
      for (srcnum = 0; srcnum < nsources; srcnum++) {
         emap.src = source + srcnum;
         portCnt = 0;
         do {
            emap.port = emap.src -> sflags & SDISTANT 
                        ? photonPorts + portCnt : NULL;
            photonPartition [emap.src -> so -> otype] (&emap);
            if (photonRepTime) {
               sprintf(errmsg, "PREPASS %d on source %s ", 
                       pass, source [srcnum].so -> oname);
               if (emap.port) {
                  sprintf(errmsg2, "via port %s ", 
                          photonPorts [portCnt].so -> oname);
                  strcat(errmsg, errmsg2);
               }
               sprintf(errmsg2, "(%lu partitions)...\n", emap.numPartitions);
               strcat(errmsg, errmsg2);
               eputs(errmsg);
               fflush(stderr);
            }
            for (emap.partitionCnt = 0; 
                 emap.partitionCnt < emap.numPartitions;
                 emap.partitionCnt++) {             
               photonOrigin [emap.src -> so -> otype] (&emap);
               /* Build emission map for current source partishunn */
               initEmissionMap(&emap);
               /* Number of photons to emit from this partishunn */
               ec = preDistrib * numEmit * 
                    colorAvg(emap.partFlux) / totalFlux;
               emitCnt = ec;
               /* Probabilistically account for fractional photons */
               if (pmapRandom(cntState) < ec - emitCnt) emitCnt++;
               while (emitCnt) {                   
                  /* Choose photon direction based on PDF */
                  emitPhoton(&emap, &photonRay);
                  tracePhoton(&photonRay);
                  /* record progress, mon */
                  repProgress++;
                  emitCnt--;
                  if (photonRepTime > 0 && 
                      time(NULL) >= repLastTime + photonRepTime)
                     distribReport();
#ifndef BSD
                  else signal(SIGCONT, distribReport);
#endif
               }
            }
            portCnt++;
         } while (portCnt < numPhotonPorts);
      }
      /* Double preDistrib in case a photon map is empty 
         --> possibility of infinite loop for pathological scenes */
      preDistrib *= 2;
   } while ( (gpm && !gpm -> heapEnd)
			 || (cpm && !cpm -> heapEnd)
			 || (vpm && !vpm -> heapEnd)
			 || (dpm && !dpm -> heapEnd) );
   /* Based on the outcome of the prepass we can now figure out how 
      many more photons we have to emit for each photon map to meet 
      its respective target count. This value is clamped to 0 in case
      the target has already been exceeded in the prepass. */
   if (gpm) 
      gpm -> distribRatio = repProgress * 
                            (max((float)gpm -> distribTarget /
                             gpm -> heapEnd, 1) - 1);
   if (cpm) 
      cpm -> distribRatio = repProgress * 
                            (max((float)cpm -> distribTarget / 
                             cpm -> heapEnd, 1) - 1);
   if (vpm) 
      vpm -> distribRatio = repProgress * 
                            (max((float)vpm -> distribTarget / 
                             vpm -> heapEnd, 1) - 1);
   if (dpm) 
      dpm -> distribRatio = repProgress * 
                            (max((float)dpm -> distribTarget / 
                             dpm -> heapEnd, 1) - 1);
   /* Number of photons to emit in main pass is the maximum required
      for all photon maps */
   repComplete = numEmit = max(gpm ? gpm -> distribRatio : 0,
                               max(cpm ? cpm -> distribRatio : 0,
                                   max(vpm ? vpm -> distribRatio : 0,
                                       dpm ? dpm -> distribRatio : 0)));
   repComplete += repProgress;
   if (numEmit) {
      /* Now set the distribution ratio for each map; this indicates
         how many photons of each respective type are stored per 
         emitted photon, and takes into account the fact that we are 
         emitting numEmit photons, which is the *maximum* required for 
         all photon maps. The photon map requiring numEmit photons will 
         have a distribution ratio of 1, while the others are < 1. 
         Since this affects the photon density, it will require flux 
         adjustment. */ 
      if (gpm) gpm -> distribRatio /= numEmit;
      if (cpm) cpm -> distribRatio /= numEmit;
      if (vpm) vpm -> distribRatio /= numEmit;
      if (dpm) dpm -> distribRatio /= numEmit;
      /* Now do the main pass */
      for (srcnum = 0; srcnum < nsources; srcnum++) {
         emap.src = source + srcnum;
         portCnt = 0;
         do {
            emap.port = emap.src -> sflags & SDISTANT 
                        ? photonPorts + portCnt : NULL;
            photonPartition [emap.src -> so -> otype] (&emap);
            if (photonRepTime) {
               sprintf(errmsg, "MAIN PASS on source %s ", 
                       source [srcnum].so -> oname);
               if (emap.port) {
                  sprintf(errmsg2, "via port %s ", 
                          photonPorts [portCnt].so -> oname);
                  strcat(errmsg, errmsg2);
               }
               sprintf(errmsg2, "(%lu partitions)...\n", emap.numPartitions);
               strcat(errmsg, errmsg2);
               eputs(errmsg);
               fflush(stderr);
            }
            for (emap.partitionCnt = 0; 
                 emap.partitionCnt < emap.numPartitions;
                 emap.partitionCnt++) {             
               photonOrigin [emap.src -> so -> otype] (&emap);
               /* Build emission map for current source partition */
               initEmissionMap(&emap);
               /* Number of photons to emit from this partition */
               ec = numEmit * colorAvg(emap.partFlux) / totalFlux;
               emitCnt = ec;
               /* Probabilistically account for fractional photons */
               if (pmapRandom(cntState) < ec - emitCnt) emitCnt++;
               while (emitCnt) {                   
                  /* Choose photon direction based on PDF */
                  emitPhoton(&emap, &photonRay);
                  tracePhoton(&photonRay);
                  repProgress++;
                  emitCnt--;
                  if (photonRepTime > 0 && 
                      time(NULL) >= repLastTime + photonRepTime)
                     distribReport();
#ifndef BSD
                  else signal(SIGCONT, distribReport);
#endif
               }
            }
            portCnt++;
         } while (portCnt < numPhotonPorts);
      }
   }
   signal(SIGCONT, SIG_DFL);
   free(emap.samples);
   /* Set photon flux (repProgress is total num emitted) */
   totalFlux /= repProgress;
   if (gpm) {
      if (photonRepTime) {
         eputs("\nBuilding global photon heap...\n");
         fflush(stderr);
      }
      balancePhotons(gpm, totalFlux);
      if (gpm -> maxGather) preComputeGlobal(gpm);
   }
   if (cpm) {
      if (photonRepTime) {
         eputs("\nBuilding caustic photon heap...\n");
         fflush(stderr);
      }
      balancePhotons(cpm, totalFlux);
   }
   if (vpm) {
      if (photonRepTime) {
         eputs("\nBuilding volume photon heap...\n");
         fflush(stderr);
      }
      balancePhotons(vpm, totalFlux);
   }
   if (dpm) {
      if (photonRepTime) {
         eputs("\nBuilding direct photon heap...\n");
         fflush(stderr);
      }
      balancePhotons(dpm, totalFlux);
   }
}


#ifndef DAYSIM
void photonDensity (PhotonMap* pmap, const RAY* ray, COLOR irrad)
#else
	void photonDensity (PhotonMap* pmap, const RAY* ray, COLOR irrad, DaysimCoef daysimCoef )
#endif
/* Photon density estimate. Returns irradiance at ray -> rop. */
{
   register unsigned i;
   register PhotonSQNode *sq;
   register float r;
   //   PhotonSQNode *squeueEnd;
   COLOR flux;
 
   setcolor(irrad, 0, 0, 0);
   DAYSIM_SET( daysimCoef, 0.0 );

   if (!pmap -> maxGather) return;
   /* Ignore sources */
   if (ray -> ro) if (islight(objptr(ray -> ro -> omod) -> otype)) return;
   pmap -> squeueEnd = 0;
   findPhotons(pmap, ray);
   /* Need at least 2 photons */
   if (pmap -> squeueEnd < 2) return;
   if (pmap -> minGather == pmap -> maxGather) {
      /* No bias compensation. Just do a plain vanilla estimate */
      sq = pmap -> squeue + 1;
      /* Average radius between furthest two photons to improve accuracy */      
      r = max(sq -> dist, (sq + 1) -> dist);
      r = 0.25 * (pmap -> maxDist + r + 2 * sqrt(pmap -> maxDist * r));   
      /* Skip the extra photon */
      for (i = 1 ; i < pmap -> squeueEnd; i++, sq++) {
         getPhotonFlux(sq -> photon, flux);
         addcolor(irrad, flux);

		 DAYSIM_ADD_COEF( daysimCoef, sq->photon->daysimSourcePatch, colval(flux,RED) );
      }
      /* Divide by search area PI * r^2, 1 / PI required as ambient 
         normalisation factor */         
      scalecolor(irrad, 1 / (PI * PI * r)); 

	  DAYSIM_SCALE( daysimCoef, 1 / (PI * PI * r));
      return;
   }
   /* Apply bias compensation to density estimate */
   else biasComp(pmap, irrad);
}



void volumePhotonDensity (PhotonMap* pmap, const RAY* ray, COLOR irrad)
/* Photon volume density estimate. Returns irradiance at ray -> rop. */
{
   register unsigned i;
   register PhotonSQNode *sq;
   register float gecc2;
   register float r, ph;
   //   PhotonSQNode *squeueEnd;
   COLOR flux;

   setcolor(irrad, 0, 0, 0);
   if (!pmap -> maxGather) return;
   pmap -> squeueEnd = 0;
   findPhotons(pmap, ray);
   /* Need at least 2 photons */
   if (pmap -> squeueEnd < 2) return;
   if (pmap -> minGather == pmap -> maxGather) {
      /* No bias compensation. Just do a plain vanilla estimate */
      gecc2 = ray -> gecc * ray -> gecc;
      sq = pmap -> squeue + 1;
      /* Average radius between furthest two photons to improve accuracy */      
      r = max(sq -> dist, (sq + 1) -> dist);
      r = 0.25 * (pmap -> maxDist + r + 2 * sqrt(pmap -> maxDist * r));   
      /* Skip the extra photon */
      for (i = 1 ; i < pmap -> squeueEnd; i++, sq++) {
         /* Compute phase function for inscattering from photon */
         if (gecc2 <= FTINY) ph = 1;
         else {
            ph = DOT(ray -> rdir, sq -> photon -> norm) / 127;
            ph = 1 + gecc2 - 2 * ray -> gecc * ph;
            ph = (1 - gecc2) / (ph * sqrt(ph));
         }
         getPhotonFlux(sq -> photon, flux);
         scalecolor(flux, ph);
         addcolor(irrad, flux);
      }
      /* Divide by search volume 4 / 3 * PI * r^3 and phase function
         normalization factor 1 / (4 * PI) */
      scalecolor(irrad, 3 / (16 * PI * PI * r * sqrt(r)));
      return;
   }
   /* Apply bias compensation to density estimate */
   else volumeBiasComp(pmap, ray, irrad);
}
