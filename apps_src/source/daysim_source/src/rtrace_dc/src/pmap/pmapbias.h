#ifndef PMAPBIASCOMP_H
#define PMAPBIASCOMP_H

/* 
   ==================================================================
   Bias compensation for photon density estimates
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapdata.h"



/* Bias compensation weighting function */
/* #define BIASCOMP_WGT(n) 1 */
/* #define BIASCOMP_WGT(n) (n) */
#define BIASCOMP_WGT(n) ((n) * (n))
/* #define BIASCOMP_WGT(n) ((n) * (n) * (n)) */
/* #define BIASCOMP_WGT(n) exp(0.003 * (n)) */

/* Dump photon bandwidth for bias compensated density estimates */
/* #define BIASCOMP_BWIDTH */



void biasComp (PhotonMap*, COLOR);
/* Photon density estimate with bias compensation, returning irradiance. 
   Expects photons in search queue after a kd-tree lookup. */

void volumeBiasComp (PhotonMap*, const RAY*, COLOR);
/* Photon volume density estimate with bias compensation, returning
   irradiance. Expects photons in search queue after a kd-tree lookup. */

char* biasCompStats (const PhotonMap*, const char* type, char* stats);
/* Return bias compensation statistics as string for specified photon map
   and type during rendering pass. Returns pointer to stats or NULL if photon
   map is not subject to bias compensation (i.e. minGather >= maxGather). */

#endif
