/* 
   ===================================================================
   Photon map support miscellany that doesn't really fit anywhere else
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ===================================================================
*/



#include "pmapmisc.h"
#include "rterror.h"

#include <string.h>
#include <ctype.h>


void colorNorm (COLOR c)
{
   register const float avg = colorAvg(c);
   
   if (!avg) return;
   c [0] /= avg;
   c [1] /= avg;
   c [2] /= avg;
}



unsigned long parseMultiplier (const char *num)
{
   unsigned long mult = 1;
   const strEnd = strlen(num) - 1;
   
   if (!isdigit(num [strEnd]))
      switch (toupper(num [strEnd])) {
         case 'G': mult *= 1000;
         case 'M': mult *= 1000;
         case 'K': mult *= 1000;
                   break;
         default : error(USER, "unknown multiplier");
      }
   return (unsigned long)(mult * atof(num));
}
