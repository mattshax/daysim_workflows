/* 
   ==================================================================
   Parameters common to all photon map modules. 
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapparm.h"
#include "standard.h"



float pdfSamples = 1000,                /* PDF samples per steradian */
      finalGather = 0.25,               /* fraction of global photons
                                           for irradiance precomputation */
      preDistrib = 0.25;                /* fraction of num photons for
                                           distribution prepass */
unsigned long photonHeapSizeInc = 1000, /* Photon heap size increment */
              photonMaxBounce = 5000;   /* Runaway photon bounce limit */
unsigned photonRepTime = 0,             /* Seconds between reports */
         maxPreDistrib = 8;             /* Max predistrib passes */

/* Per photon map params */
PhotonMapParams globalPmapParams = {NULL, 0, 0, 0, PMAP_IRRAD_THRESH},
                causticPmapParams = {NULL, 0, 0, 0, PMAP_IRRAD_THRESH},
                volumePmapParams = {NULL, 0, 0, 0, PMAP_IRRAD_THRESH},
                directPmapParams = {NULL, 0, 0, 0, PMAP_IRRAD_THRESH};
