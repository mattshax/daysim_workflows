/* 
   ===============================================================   
   Photon map query utility

   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ===============================================================   
*/



#include "pmapio.h"

#include "rtio.h"

/*
extern char* fgetline (char*, int, FILE*);
extern char* getstr (char*, FILE*);
extern long getint (int, FILE*);
extern double getflt (FILE*);
*/


static char header [] = "$Revision: 1.16 $";



int main (int argc, char** argv)
{
   char buf [PMAP_CMDLINE_MAX];
   FILE* pmapFile;
   PhotonMap pmap;
   COLOR flux;
   
   if (argc != 2) {
      printf("%s <photonmap_file>\n", argv [0]);
      return 1;
   }
   if (!(pmapFile = fopen(argv [1], "rb"))) {
      printf("Cannot open photon map file %s\n", argv [1]);
      return 1;
   }
   /* Magic numbah */
   getstr(buf, pmapFile);
   if (strcmp(buf, GLOBAL_PMAP_MAGIC) && 
       strcmp(buf, PRECOMP_GLOBAL_PMAP_MAGIC) &&
       strcmp(buf, CAUSTIC_PMAP_MAGIC) &&
       strcmp(buf, VOLUME_PMAP_MAGIC) &&
       strcmp(buf, DIRECT_PMAP_MAGIC)) {
      puts("Corrupt photon map file");
      return 1;
   }
   printf("%s\n", buf);
   /* Command line */
   fgetline(buf, sizeof(buf), pmapFile);
   printf("%s\n", buf);
   /* Number of photons */
   printf("%ld photons\n", getint(sizeof(pmap.heapSize), pmapFile));
   flux [0] = getflt(pmapFile);
   flux [1] = getflt(pmapFile);
   flux [2] = getflt(pmapFile);
   printf("[%E, %E, %E] avg watts / photon\n", 
          flux [0], flux [1], flux [2]);
   fclose(pmapFile);
   return 0;
}
