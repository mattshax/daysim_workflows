OPT = -O
MACH = -DBSD
CFLAGS = -I. -I../common -I../rt -L../lib $(OPT) $(MACH)
CC = cc
MLIB = -lm

#
# The following are user-definable:
#
DESTDIR = .
INSTDIR = /usr/local/bin
INSTALL = mv

#
# The following paths must exist and be relative to root:
#
LIBDIR = /usr/local/lib/ray

#
# Library routines:
#
PMLIB = ../lib/libpmap.a
LIBS = -lrtrad $(MLIB)

PMOBJS = pmap.o pmapsrc.o pmapmat.o pmaprand.o pmapio.o pmapdata.o \
         pmapbias.o pmapmisc.o pmapparm.o
PMSRC = pmap.c pmapsrc.c pmapmat.c pmaprand.c pmapio.c pmapdata.c \
        pmapbias.c pmapmisc.c pmapparm.c
PMDEPS = pmap.dep pmapsrc.dep pmapmat.dep pmaprand.dep pmapio.dep \
         pmapdata.dep pmapbias.dep pmapmisc.dep pmapparm.dep pmapinfo.dep \
         pmapdump.dep

#
# What this makefile produces:
#
PROGS = $(DESTDIR)/pmapinfo $(DESTDIR)/pmapdump

all:	$(PROGS) $(PMLIB)

install: all
	$(INSTALL) $(PROGS) $(INSTDIR)

clean:
	set nonomatch; rm -f $(PROGS) *.o core

dep: $(PMDEPS)

%.dep: %.c
	$(CC)  -MM -MF $@ $(CFLAGS) $<

#
# Links:
#
$(DESTDIR)/pmapinfo: pmapinfo.o
	$(CC) $(CFLAGS) -o $(DESTDIR)/pmapinfo pmapinfo.o $(LIBS)

$(DESTDIR)/pmapdump: pmapdump.o
	$(CC) $(CFLAGS) -o $(DESTDIR)/pmapdump pmapdump.o $(LIBS)

$(PMLIB): $(PMOBJS)
	ar rc $(PMLIB) $(PMOBJS)
	-ranlib $(PMLIB)

#
# Dependencies:
#
pmapbias.o: pmapbias.c pmapbias.h pmapdata.h ../rt/ray.h \
  ../common/standard.h ../common/copyright.h ../common/rtio.h \
  ../common/rtmisc.h ../common/rtmath.h ../common/tifftypes.h \
  ../common/mat4.h ../common/fvect.h ../common/rterror.h \
  ../common/octree.h ../common/object.h ../common/color.h pmaprand.h \
  pmapmisc.h
pmapdata.o: pmapdata.c pmapdata.h ../rt/ray.h ../common/standard.h \
  ../common/copyright.h ../common/rtio.h ../common/rtmisc.h \
  ../common/rtmath.h ../common/tifftypes.h ../common/mat4.h \
  ../common/fvect.h ../common/rterror.h ../common/octree.h \
  ../common/object.h ../common/color.h pmaprand.h pmapmisc.h \
  ../common/otypes.h
pmap.o: pmap.c pmap.h pmapparm.h pmapdata.h ../rt/ray.h \
  ../common/standard.h ../common/copyright.h ../common/rtio.h \
  ../common/rtmisc.h ../common/rtmath.h ../common/tifftypes.h \
  ../common/mat4.h ../common/fvect.h ../common/rterror.h \
  ../common/octree.h ../common/object.h ../common/color.h pmapmat.h \
  pmapsrc.h ../rt/source.h pmaprand.h pmapio.h pmapmisc.h pmapbias.h \
  ../common/otypes.h
pmapdump.o: pmapdump.c pmapio.h pmapdata.h ../rt/ray.h \
  ../common/standard.h ../common/copyright.h ../common/rtio.h \
  ../common/rtmisc.h ../common/rtmath.h ../common/tifftypes.h \
  ../common/mat4.h ../common/fvect.h ../common/rterror.h \
  ../common/octree.h ../common/object.h ../common/color.h
pmapinfo.o: pmapinfo.c pmapio.h pmapdata.h ../rt/ray.h \
  ../common/standard.h ../common/copyright.h ../common/rtio.h \
  ../common/rtmisc.h ../common/rtmath.h ../common/tifftypes.h \
  ../common/mat4.h ../common/fvect.h ../common/rterror.h \
  ../common/octree.h ../common/object.h ../common/color.h
pmapio.o: pmapio.c pmapio.h pmapdata.h ../rt/ray.h ../common/standard.h \
  ../common/copyright.h ../common/rtio.h ../common/rtmisc.h \
  ../common/rtmath.h ../common/tifftypes.h ../common/mat4.h \
  ../common/fvect.h ../common/rterror.h ../common/octree.h \
  ../common/object.h ../common/color.h pmapparm.h
pmapmat.o: pmapmat.c pmapmat.h pmap.h pmapparm.h pmapdata.h ../rt/ray.h \
  ../common/standard.h ../common/copyright.h ../common/rtio.h \
  ../common/rtmisc.h ../common/rtmath.h ../common/tifftypes.h \
  ../common/mat4.h ../common/fvect.h ../common/rterror.h \
  ../common/octree.h ../common/object.h ../common/color.h pmaprand.h \
  pmapmisc.h ../common/otypes.h ../rt/data.h ../rt/func.h \
  ../common/calcomp.h
pmapmisc.o: pmapmisc.c pmapmisc.h ../common/standard.h \
  ../common/copyright.h ../common/rtio.h ../common/rtmisc.h \
  ../common/rtmath.h ../common/tifftypes.h ../common/mat4.h \
  ../common/fvect.h ../common/rterror.h ../common/color.h
pmapparm.o: pmapparm.c pmapparm.h ../common/standard.h \
  ../common/copyright.h ../common/rtio.h ../common/rtmisc.h \
  ../common/rtmath.h ../common/tifftypes.h ../common/mat4.h \
  ../common/fvect.h ../common/rterror.h
pmaprand.o: pmaprand.c
pmapsrc.o: pmapsrc.c pmapsrc.h ../rt/ray.h ../common/standard.h \
  ../common/copyright.h ../common/rtio.h ../common/rtmisc.h \
  ../common/rtmath.h ../common/tifftypes.h ../common/mat4.h \
  ../common/fvect.h ../common/rterror.h ../common/octree.h \
  ../common/object.h ../common/color.h ../rt/source.h pmapparm.h \
  pmaprand.h pmapmisc.h ../common/otypes.h
