#ifndef PMAP_H
#define PMAP_H

/* 
   ==================================================================
   Photon map main header
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapparm.h"
#include "pmapdata.h"



void setPmapParams (PhotonMap**, const PhotonMapParams*,
                    PhotonMap**, const PhotonMapParams*,
                    PhotonMap**, const PhotonMapParams*,
                    PhotonMap**, const PhotonMapParams*);
/* Set up & initialise photon maps according to their respective 
   parameters */

void loadPmaps (PhotonMap*, PhotonMap*, PhotonMap*, PhotonMap*);
/* Load all defined photon maps, checking timestamps relative to octree 
   for possible staleness */

void savePmaps (PhotonMap*, PhotonMap*, PhotonMap*, PhotonMap*, char*);
/* Save all defined photon maps with specified command line */

void cleanUpPmaps (PhotonMap*, PhotonMap*, PhotonMap*, PhotonMap*);
/* Trash all photon maps after processing is complete */

void distribPhotons (PhotonMap*, PhotonMap*, PhotonMap*, PhotonMap*);
/* Emit photons from light sources and build photon maps for the specified
   non-NULL parameters */

void tracePhoton (RAY*);
/* Follow photon as it bounces around the scene. Analogon to raytrace(). */

#ifndef DAYSIM
void photonDensity (PhotonMap*, const RAY*, COLOR);
/* Perform surface density estimate from incoming photon flux at
   ray's intersection point. Returns irradiance from found photons. */
#else
extern void photonDensity( PhotonMap*, const RAY*, COLOR, DaysimCoef );
#endif
   
void volumePhotonDensity (PhotonMap*, const RAY*, COLOR);
/* Perform volume density estimate from incoming photon flux at 
   ray's intersection point. Returns irradiance. */

#endif
