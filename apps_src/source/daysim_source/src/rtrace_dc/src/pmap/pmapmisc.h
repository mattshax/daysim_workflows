#ifndef PMAPMISC_H
#define PMAPMISC_H

/* 
   ===================================================================
   Photon map support miscellany that doesn't really fit anywhere else
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ===================================================================
*/



#include "standard.h"
#include "color.h"



#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

/* Average over colour channels */
#define colorAvg(col) ((col [0] + col [1] + col [2]) / 3)



extern void colorNorm (COLOR);
/* Normalise colour channels so average is 1 */

extern unsigned long parseMultiplier (const char *num);
/* Evaluate numeric parameter string with optional multiplier suffix
   (G = 10^9, M = 10^6, K = 10^3). Returns 0 if parsing fails. */

#endif
