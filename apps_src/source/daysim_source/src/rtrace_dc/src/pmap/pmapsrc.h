#ifndef PMAPSRC_H
#define PMAPSRC_H



/* 
   ==================================================================
   Photon map support routines for emission from light sources.
   Nu stuff, babee!
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "ray.h"
#include "source.h"



/* Data structures for photon emission */
typedef struct {
   unsigned theta, phi;               /* Direction indices */
   COLOR pdf;                         /* Probability of emission in this 
                                         direction per RGB */
   float cdf;                         /* Cumulative probability up to
                                         this sample */
} EmissionSample;

typedef struct {
   unsigned numTheta, numPhi;         /* Num divisions in theta & phi */
   RREAL cosThetaMax;                 /* cos(source aperture) */
   FVECT uh, vh, wh;                  /* Emission aperture axes at origin, 
                                         w is normal*/
   FVECT us, vs, ws;                  /* Source surface axes at origin, 
                                         w is normal */
   FVECT photonOrg;                   /* Current photon origin */
   EmissionSample *samples;           /* PDF samples for photon emission 
                                         directions */
   unsigned long numPartitions;       /* Number of surface partitions */
   RREAL partArea;                    /* Area covered by each partition */
   SRCREC *src, *port;                /* Current source and port (if used) */
   unsigned long partitionCnt,        /* Current surface partition */
                 maxPartitions,       /* Max allocated partitions */
                 numSamples;          /* Number of PDF samples */
   unsigned char* partitions;         /* Source surface partitions */
   COLOR partFlux;                    /* Total flux emitted by partition */
   float cdf;                         /* Cumulative probability */
} EmissionMap;



/* Dispatch tables for partitioning light source surfaces and generating
   an origin for photon emission */
extern void (*photonPartition []) (EmissionMap*);
extern void (*photonOrigin []) (EmissionMap*);



extern void initPhotonEmissionFuncs ();
/* Init photonPartition[] and photonOrigin[] dispatch tables with source
   specific emission routines */

extern void sourceFlux (EmissionMap*);
/* Return flux emitted from current source partition */

void initEmissionMap (EmissionMap*);
/* Init photon emission map from partitioned light source for current 
   photon origin */

void emitPhoton (const EmissionMap*, RAY* ray);
/* Emit photon from current source partition based on emission 
   distribution and return new photon ray */

#endif
