/* 
   ==================================================================
   Random number generators for photon distribution
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



/* Separate RNG states are used for the following variates during photon 
   distribution:
   - source partition
   - emission direction
   - emission counter fraction
   - mean free distance in medium
   - scattering direction
   - russian roulette.
   Each photon map also has a local state randState used for distribRatio 
   during distribution and for bias compensation during gathering. */
   
unsigned short partState [3] = {47717, 5519, 21521},
               emitState [3] = {33997, 59693, 11003},
               cntState [3] = {17077, 4111, 48907},
               mediumState [3] = {25247, 7507, 33797},
               scatterState [3] = {21863, 45191, 5099},
               rouletteState [3] = {10243, 39829, 9433};
