/* 
   ==================================================================
   Photon map file I/O and diagnostics
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapio.h"
#include "pmapparm.h"
#ifndef NON_POSIX
#ifdef BSD
#include <sys/time.h>
#include <sys/resource.h>
#else
#include <sys/times.h>
#include <unistd.h>
#endif
#endif
#include <signal.h>



time_t repStartTime, repLastTime = 0;   /* Time at start & last report */
unsigned long repProgress,              /* Report progress counter */
              repComplete;              /* Report completion count */



void savePhotonMap (const PhotonMap* pmap, const char* fname,
                    char* command, const char* magic)
{
   register unsigned long i, j;
   const Photon* p;
   FILE* file;

   if (!pmap -> heapSize) return;
   if (photonRepTime) {
      sprintf(errmsg, "Saving %s (%ld photons)...\n", 
              fname, pmap -> heapSize);
      eputs(errmsg);
      fflush(stderr);
   }
   if (!(file = fopen(fname, "wb"))) 
      error(USER, "can't open photon map file");
   /* Write magic numbah */
   putstr((char*)magic, file);
   /* Write command line, truncating if too long */
   if (strlen(command) >= PMAP_CMDLINE_MAX)
      command [PMAP_CMDLINE_MAX - 1] = '\0';
   putstr((char*)command, file);
   fputc('\n', file);
   /* Write number of photons */
   putint(pmap -> heapSize, sizeof(pmap -> heapSize), file);
   /* Write average photon flux */
   for (j = 0; j < 3; j++) putflt(pmap -> photonFlux [j], file);
   for (i = 0, p = pmap -> heap; i < pmap -> heapSize; i++, p++) {
      /* Write photon attributes */
      for (j = 0; j < 3; j++) putflt(p -> pos [j], file);
      /* Bytewise dump otherwise we have portability probs */
      for (j = 0; j < 3; j++) putint(p -> norm [j], 1, file);
#ifdef PMAP_FLOAT_FLUX
      for (j = 0; j < 3; j++) putflt(p -> flux [j], file);
#else
      for (j = 0; j < 4; j++) putint(p -> flux [j], 1, file);
#endif
      putint(p -> discr, 1, file);

#ifdef DAYSIM
	  putint( p->daysimSourcePatch, 1, file );
#endif

      if (ferror(file)) error(USER, "error writing photon map file");
   }
   fclose(file);
}



void loadPhotonMap (PhotonMap* pmap, const char* fname,
                    char* command, char* magic)
{
   char buf [PMAP_CMDLINE_MAX];
   Photon* p;
   register unsigned long i, j;
   FILE *file;

   if ((file = fopen(fname, "rb")) == NULL)
      error(USER, "can't open photon map file");
   /* Check magic numbah */
   getstr(buf, file);
   if (!magic [0]) strcpy(magic, buf);
   else if (strcmp(buf, magic)) 
      error(USER, "corrupt or mismatched photon map file");
   /* Get command line */
   fgetline(buf, sizeof(buf), file);
   if (command) strcpy(command, buf);
   initPhotonMap(pmap);
   /* Get number of photons */
   pmap -> heapSize = pmap -> heapEnd = 
      getint(sizeof(pmap -> heapSize), file);
   pmap -> heap = (Photon*)malloc(pmap -> heapSize * sizeof(Photon));
   if (!pmap -> heap) error(USER, "can't allocate photon heap");
   /* Get average photon flux */
   for (j = 0; j < 3; j++) pmap -> photonFlux [j] = getflt(file);
   for (i = 0, p = pmap -> heap; i < pmap -> heapSize; i++, p++) {
      /* Get photon attributes */
      for (j = 0; j < 3; j++) p -> pos [j] = getflt(file);
      /* Bytewise grab otherwise we have portability probs */
      for (j = 0; j < 3; j++) p -> norm [j] = getint(1, file);
#ifdef PMAP_FLOAT_FLUX
      for (j = 0; j < 3; j++) p -> flux [j] = getflt(file);
#else      
      for (j = 0; j < 4; j++) p -> flux [j] = getint(1, file);
#endif
      p -> discr = getint(1, file);

#ifdef DAYSIM
	  p->daysimSourcePatch= getint( 1, file );
#endif

      if (feof(file)) error(USER, "error reading photon map file");
   }
   fclose(file);
}



#ifndef NON_POSIX
void preComputeReport()
/* Report global photon precomputation progress */
{
   extern char *myhostname();
   float u, s;
   char errmsg2 [512];
#ifdef BSD
   struct rusage rubuf;
#else
   struct tms tbuf;
   float period;
#endif

   repLastTime = time(NULL);
#ifdef BSD
   getrusage(RUSAGE_SELF, &rubuf);
   u = rubuf.ru_utime.tv_sec + rubuf.ru_utime.tv_usec / 1e6;
   s = rubuf.ru_stime.tv_sec + rubuf.ru_stime.tv_usec / 1e6;
   getrusage(RUSAGE_CHILDREN, &rubuf);
   u += rubuf.ru_utime.tv_sec + rubuf.ru_utime.tv_usec / 1e6;
   s += rubuf.ru_stime.tv_sec + rubuf.ru_stime.tv_usec / 1e6;
#else
   times(&tbuf);
#ifdef _SC_CLK_TCK
   period = 1.0 / sysconf(_SC_CLK_TCK);
#else
   period = 1.0 / 60;
#endif
   u = (tbuf.tms_utime + tbuf.tms_cutime) * period;
   s = (tbuf.tms_stime + tbuf.tms_cstime) * period;
#endif
   sprintf(errmsg, "%lu precomputed, ", repProgress);
   if (globalPmap -> maxGather > globalPmap -> minGather) {
      /* Report bias compensation stats */
      sprintf(errmsg2, 
              "%d/%d/%d global (%.1f/%.1f/%.1f%% error), ",
              globalPmap -> minGathered, globalPmap -> maxGathered, 
              globalPmap -> numDensity ? globalPmap -> totalGathered /
                                         globalPmap -> numDensity
                                       : 0,
              100 * globalPmap -> minError, 100 * globalPmap -> maxError,
              globalPmap -> numDensity ? 100 * sqrt(globalPmap -> rmsError /
                                                    globalPmap -> numDensity)
                                       : 0);
      strcat(errmsg, errmsg2);
   }    
   sprintf(errmsg2, "%4.2f%% after %.3fu %.3fs %.3fr hours on %s\n",
           100.0 * repProgress / repComplete, u / 3600, s / 3600, 
           (repLastTime - repStartTime) / 3600.0, myhostname());
   strcat(errmsg, errmsg2);
   eputs(errmsg);
   fflush(stderr);
#ifndef BSD
   signal(SIGCONT, preComputeReport);
#endif
}



void distribReport()
/* Report photon distribution progress */
{
   extern char *myhostname();
   float u, s;
   char errmsg2 [512];
#ifdef BSD
   struct rusage rubuf;
#else
   struct tms tbuf;
   float period;
#endif

   repLastTime = time(NULL);
#ifdef BSD
   getrusage(RUSAGE_SELF, &rubuf);
   u = rubuf.ru_utime.tv_sec + rubuf.ru_utime.tv_usec / 1e6;
   s = rubuf.ru_stime.tv_sec + rubuf.ru_stime.tv_usec / 1e6;
   getrusage(RUSAGE_CHILDREN, &rubuf);
   u += rubuf.ru_utime.tv_sec + rubuf.ru_utime.tv_usec / 1e6;
   s += rubuf.ru_stime.tv_sec + rubuf.ru_stime.tv_usec / 1e6;
#else
   times(&tbuf);
#ifdef _SC_CLK_TCK
   period = 1.0 / sysconf(_SC_CLK_TCK);
#else
   period = 1.0 / 60;
#endif
   u = (tbuf.tms_utime + tbuf.tms_cutime) * period;
   s = (tbuf.tms_stime + tbuf.tms_cstime) * period;
#endif
   sprintf(errmsg, "%lu emitted, ", repProgress);
   if (globalPmap) {
      sprintf(errmsg2, "%lu global, ", globalPmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   if (causticPmap) {
      sprintf(errmsg2, "%lu caustic, ", causticPmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   if (volumePmap) {
      sprintf(errmsg2, "%lu volume, ", volumePmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   if (directPmap) {
      sprintf(errmsg2, "%lu direct, ", directPmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   sprintf(errmsg2, "%4.2f%% after %.3fu %.3fs %.3fr hours on %s\n", 
           100.0 * repProgress / repComplete, u / 3600, s / 3600, 
           (repLastTime - repStartTime) / 3600.0, myhostname());
   strcat(errmsg, errmsg2);
   eputs(errmsg);
   fflush(stderr);
#ifndef BSD
   signal(SIGCONT, distribReport);
#endif
}



#else
void preComputeReport()
/* Report global photon precomputation progress */
{
   repLastTime = time(NULL);
   sprintf(errmsg, "%lu precomputed, ", repProgress);
   if (globalPmap -> maxGather > globalPmap -> minGather) {
      /* Report bias compensation stats */
      sprintf(errmsg2, 
              "%d/%d/%d global (%.1f/%.1f/%.1f%% error), "
              globalPmap -> minGathered, globalPmap -> maxGathered, 
              globalPmap -> numDensity ? globalPmap -> totalGathered /
                                         globalPmap -> numDensity
                                       : 0,
              100 * globalPmap -> minError, 100 * globalPmap -> maxError,
              globalPmap -> numDensity ? 100 * sqrt(globalPmap -> rmsError /
                                                    globalPmap -> numDensity)
                                       : 0);
      strcat(errmsg, errmsg2);
   }
   sprintf(errmsg2, "%4.2f%% after %5.4f hours\n", 
           100.0 * repProgress / repComplete, 
           (repLastTime - repStartTime) / 3600.0);
   strcat(errmsg, errmsg2);
   eputs(errmsg);
   fflush(stderr);
}



void distribReport()
/* Report photon distribution progress */
{
   char errmsg2 [512];
   
   repLastTime = time(NULL);
   sprintf(errmsg, "%lu emitted, ", repProgress);
   if (globalPmap) {
      sprintf(errmsg2, "%lu global, ", globalPmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   if (causticPmap) {
      sprintf(errmsg2, "%lu caustic, ", causticPmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   if (volumePmap) {
      sprintf(errmsg2, "%lu volume, ", volumePmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   if (directPmap) {
      sprintf(errmsg2, "%lu direct, ", directPmap -> heapEnd);
      strcat(errmsg, errmsg2);
   }
   sprintf(errmsg2, "%4.2f%% after %5.4f hours\n", 
           100.0 * repProgress / repComplete, 
           (repLastTime - repStartTime) / 3600.0);
   strcat(errmsg, errmsg2);
   eputs(errmsg);
   fflush(stderr);
}
#endif
