#ifndef PMAPIO_H
#define PMAPIO_H

/* 
   ==================================================================
   Photon map file I/O and diagnostics
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapdata.h"



/* Magic numbahs for photon map files */
#define GLOBAL_PMAP_MAGIC "Radiance_Global_Photon_Map"   
#define PRECOMP_GLOBAL_PMAP_MAGIC "Radiance_PreComp_Global_Photon_Map"
#define CAUSTIC_PMAP_MAGIC "Radiance_Caustic_Photon_Map"
#define VOLUME_PMAP_MAGIC "Radiance_Volume_Photon_Map"
#define DIRECT_PMAP_MAGIC "Radiance_Direct_Photon_Map"

/* Max command line length in photon map files */
#define PMAP_CMDLINE_MAX 1024



/* Time at start & last report */
extern time_t repStartTime, repLastTime;   
/* Report progress & completion counters */
extern unsigned long repProgress, repComplete;              



extern void savePhotonMap (const PhotonMap* pmap, const char* fname,
                           char* command, const char* magic);
/* Save portable photon map with specified filename and magic numbah,
   along with the corresponding command line. */

extern void loadPhotonMap (PhotonMap* pmap, const char* fname,
                           char* command, char* magic);                           
/* Load portable photon map with specified filename. If magic is specified 
   it is used to check for a valid photon map type. If magic is an empty
   string, it is set to the file's magic string. Returns command line used
   to generate the file if command != NULL. */

extern void distribReport ();
/* Report photon distribution progress */

extern void preComputeReport ();
/* Report global photon precomputation progress */

#endif
