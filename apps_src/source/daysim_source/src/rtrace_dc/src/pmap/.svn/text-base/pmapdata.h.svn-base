#ifndef PMAPDATA_H
#define PMAPDATA_H

/* 
   ==================================================================
   Photon map data structures and kd-tree handling   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "ray.h"

#ifdef DAYSIM
#include "daysim.h"
#endif


/* Define PMAP_FLOAT_FLUX to store photon flux as floats instead of compact
   RGBE, which was found to improve accuracy in analytical validation. */

#ifdef PMAP_FLOAT_FLUX
#define setPhotonFlux(p, f) copycolor((p) -> flux, f)
#define getPhotonFlux(p, f) copycolor(f, (p) -> flux)
#else
#define setPhotonFlux(p, f) setcolr((p) -> flux, (f) [0], (f) [1], (f) [2])
#define getPhotonFlux(p, f) colr_color(f, (p) -> flux)
#endif



typedef struct {
   float pos [3];                 /* Photon position */
   signed char norm [3];          /* Surface normal at pos (incident
                                     direction for volume photons) */
   char discr;                    /* kd-tree discriminator */
#ifdef PMAP_FLOAT_FLUX
   COLOR flux;
#else
   COLR flux;                     /* Photon flux */
#endif

#ifdef DAYSIM					  /* annotate the source of the photon */
   DaysimSourcePatch daysimSourcePatch;
#endif
} Photon;

typedef struct {                  /* Queue node for photon searching */
   Photon* photon;
   float dist;
} PhotonSQNode;

typedef struct {                  /* Node for bias compensation history */
   COLOR irrad;
   float weight;
} PhotonBCNode;

typedef struct {
   char *fileName;                /* Photon map file */
   Photon* heap;                  /* Photon k-d tree implemented as heap */
   PhotonSQNode* squeue;          /* Search queue */
   PhotonBCNode* biasCompHist;    /* Bias compensation history */
   unsigned long distribTarget,   /* Num stored specified by user */
                 heapSize,
                 heapSizeInc, 
                 heapEnd,         /* Num actually stored in heap */
                 numDensity,      /* Num density estimates */
                 totalGathered;   /* Total photons gathered */
   unsigned minGather,            /* Specified min/max photons per */
            maxGather,            /* density estimate */
            squeueSize,
            squeueEnd,
            minGathered,          /* Min/max photons actually gathered */
            maxGathered;          /* per density estimate */
   float maxInitDist,             /* Initial NN max search radius */
         maxDist,                 /* NN max search radius during search */
         maxPos [3], minPos [3],  /* Max & min photon positions */
         distribRatio,            /* Probability of photon storage */
         minError, maxError,      /* Min/max/rms density estimate error */
         rmsError;
   COLOR photonFlux;              /* Average photon flux */
   unsigned short randState [3];  /* Local RNG state */
} PhotonMap;



extern PhotonMap *globalPmap, *causticPmap, *volumePmap, *directPmap;



extern void initPhotonMap (PhotonMap* pmap);
/* Initialise empty photon map */

extern const Photon* addPhoton (PhotonMap*, const RAY*);
/* Create new photon with ray's direction, intersection point, and flux,
   and add to photon map */

extern void balancePhotons (PhotonMap*, double photonFlux);
/* Build a balanced kd-tree as heap to guarantee logarithmic search times. 
   This must be called prior to performing photon search with 
   findPhotons(..). photonFlux is the flux per photon averaged over RGB. */
   
extern void findPhotons (PhotonMap* pmap, const RAY* ray);
/* Find pmap -> squeueSize closest photons to ray -> rop with similar 
   normal. For volume photons ray -> rorg is used and the normal is 
   ignored (being the incident direction in this case). Found photons 
   are placed in pmap -> squeue, with pmap -> squeueEnd being the number 
   actually found. */

extern Photon* findGlobal (const RAY* ray);
/* Finds single closest global photon to ray -> rop with similar normal. 
   Returns NULL if none found. */

extern void deletePhotons (PhotonMap*);
/* Free dem mammaries... */

#endif
