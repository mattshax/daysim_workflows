/* 
   ===============================================================   
   Dump photon maps as RADIANCE scene description to stdout

   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ===============================================================      
*/



#include "pmapio.h"



extern char* getstr (char*, FILE*);
extern long getint (int, FILE*);
extern double getflt (FILE*);



/* RADIANCE material and object defs for each photon type */
typedef struct {
   char *magic, *mat, *obj;
} RadianceDef;


   
static char header [] = "$Revision: 1.4 $";
/* Colour code is as follows:
   global:         blue
   precomp global: cyan
   caustic:        red
   volume:         green
   direct:         magenta */
const RadianceDef radDefs [5] = 
   {{GLOBAL_PMAP_MAGIC,
     "void plastic mat.global\n0\n0\n5 0 0 1 0 0",
     "mat.global sphere obj.global\n0\n0\n4 %g %g %g %g\n"},
    {PRECOMP_GLOBAL_PMAP_MAGIC,
     "void plastic mat.pglobal\n0\n0\n5 0 1 1 0 0",
     "mat.pglobal sphere obj.global\n0\n0\n4 %g %g %g %g\n"},
    {CAUSTIC_PMAP_MAGIC,
     "void plastic mat.caustic\n0\n0\n5 1 0 0 0 0",
     "mat.caustic sphere obj.caustic\n0\n0\n4 %g %g %g %g\n"},
    {VOLUME_PMAP_MAGIC,
     "void plastic mat.volume\n0\n0\n5 0 1 0 0 0",
     "mat.volume sphere obj.volume\n0\n0\n4 %g %g %g %g\n"},
    {DIRECT_PMAP_MAGIC,
     "void plastic mat.direct\n0\n0\n5 1 0 1 0 0",
     "mat.direct sphere obj.direct\n0\n0\n4 %g %g %g %g\n"}};



int main (int argc, char** argv)
{
   char buf [PMAP_CMDLINE_MAX];
   RREAL r;
   unsigned i, j, ptype;
   unsigned long numPhotons;
   FILE *pmapFile;
   FVECT pos;
   
   if (argc < 3) {
      puts("Dump photon maps as RADIANCE scene description\n");
      printf("Usage: %s <radius> pmap1 pmap2 ...\n", argv [0]);
      return 1;
   }
   r = atof(argv [1]);
   for (i = 2; i < argc; i++) {
      if (!(pmapFile = fopen(argv [i], "rb")))
         error(USER, "can't open photon map file");
      /* Get magic string */
      getstr(buf, pmapFile);
      /* Identify pmap type and dump material def */
      for (ptype = 0; 
           strcmp(radDefs [ptype].magic, buf) && ptype < 5; ptype++);
      if (ptype == 5) error(USER, "unknown photon map file");
      puts(radDefs [ptype].mat);
      /* Skip command line */
      fgetline(buf, sizeof(buf), pmapFile);
      /* Get number of photons */
      numPhotons = getint(sizeof(numPhotons), pmapFile);
      /* Skip average photon flux */
      for (j = 0; j < 3; j++) getflt(pmapFile);
      while (numPhotons--) {       
         /* Dump photon */
         for (j = 0; j < 3; j++) pos [j] = getflt(pmapFile);
         printf(radDefs [ptype].obj, pos [0], pos [1], pos [2], r);
         /* Skip photon normal, flux, and discriminator */
         for (j = 0; j < 3; j++) getint(1, pmapFile);
#ifdef PMAP_FLOAT_FLUX
         for (j = 0; j < 3; j++) getflt(pmapFile);
#else      
         for (j = 0; j < 4; j++) getint(1, pmapFile);
#endif
         getint(1, pmapFile);
         if (feof(pmapFile)) error(USER, "error reading photon map file");
      }
      fclose(pmapFile);
   }
   return 0;
}
