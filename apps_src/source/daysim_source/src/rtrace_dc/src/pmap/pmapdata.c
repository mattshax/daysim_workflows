/* 
   ==================================================================
   Photon map data structures and kd-tree handling
   
   Roland Schregle (ganjatron@gmx.net, roland.schregle@taganga.de)
   (c) Fraunhofer Institute for Solar Energy Systems
   ==================================================================
*/



#include "pmapdata.h"
#include "pmaprand.h"
#include "pmapmisc.h"
#include "otypes.h"



PhotonMap *globalPmap = NULL, *causticPmap = NULL, 
          *volumePmap = NULL, *directPmap = NULL;



void initPhotonMap (PhotonMap* pmap)
/* Init photon map 'n' stuff... */
{
   if (!pmap) return;
   pmap -> heapSize = pmap -> heapEnd = 0;
   pmap -> heap = NULL;
   pmap -> squeue = NULL;
   pmap -> biasCompHist = NULL;
   pmap -> maxPos [0] = pmap -> maxPos [1] = pmap -> maxPos [2] = -FHUGE;
   pmap -> minPos [0] = pmap -> minPos [1] = pmap -> minPos [2] = FHUGE;
   pmap -> minGathered = pmap -> maxGathered = pmap -> totalGathered = 0;
   pmap -> minError = pmap -> maxError = pmap -> rmsError = 0;
   /* Init local RNG state */
   pmap -> randState [0] = 10243;
   pmap -> randState [1] = 39829;
   pmap -> randState [2] = 9433;
   pmapSeed(25999, pmap -> randState);
}



const Photon* addPhoton (PhotonMap* pmap, const RAY* ray)
{
   register unsigned i;
   Photon* photon;
   COLOR photonFlux;
   
   /* Account for distribution ratio */
   if (!pmap || pmapRandom(pmap -> randState) > pmap -> distribRatio) 
      return NULL;
   /* Don't store on sources */
   if (ray -> robj > -1 && islight(objptr(ray -> ro -> omod) -> otype)) 
      return NULL;
   if (pmap -> heapEnd >= pmap -> heapSize) {
      /* Enlarge heap */
      pmap -> heapSize += pmap -> heapSizeInc;
      pmap -> heap = (Photon*)realloc(pmap -> heap, 
                                      sizeof(Photon) * pmap -> heapSize);
      if (!pmap -> heap) error(USER, "can't allocate photon heap");
   } 
   photon = pmap -> heap + pmap -> heapEnd++;
   copycolor(photonFlux, ray -> rcol);
   /* Adjust flux according to distribution ratio and ray weight */
   scalecolor(photonFlux, 
              ray -> rweight * (pmap -> distribRatio 
                                ? 1 / pmap -> distribRatio : 1));
   setPhotonFlux(photon, photonFlux);
#ifdef DAYSIM
   photon->daysimSourcePatch= ray->daysimSourcePatch;
#endif
   VCOPY(photon -> pos, ray -> rop);
   /* Update min and max positions & set normal */
   for (i = 0; i <= 2; i++) {
      if (photon -> pos [i] < pmap -> minPos [i]) 
         pmap -> minPos [i] = photon -> pos [i];
      if (photon -> pos [i] > pmap -> maxPos [i]) 
         pmap -> maxPos [i] = photon -> pos [i];
      photon -> norm [i] = 127 * (pmap == volumePmap 
                                  ? ray -> rdir [i] : ray -> ron [i]);
   }
   return photon;
}



static void nearestNeighbours (PhotonMap* pmap, const float pos [3], 
                               const float norm [3], unsigned long node)
/* Recursive part of findPhotons(..).
   Note that all heap and priority queue index handling is 1-based, but 
   accesses to the arrays are 0-based! */
{
   Photon* p = &pmap -> heap [node - 1];
   register unsigned i, j;
   /* Signed distance to current photon's splitting plane */
   register float d = pos [p -> discr] - p -> pos [p -> discr], 
                  d2 = d * d;
   register PhotonSQNode* sq = pmap -> squeue;
   register const unsigned sqSize = pmap -> squeueSize;
   float dv [3];
   
   /* Search subtree closer to pos first; exclude other subtree if the 
      distance to the splitting plane is greater than maxDist */
   if (d < 0) {
      if (node << 1 <= pmap -> heapSize) 
         nearestNeighbours(pmap, pos, norm, node << 1);
      if (d2 < pmap -> maxDist && node << 1 < pmap -> heapSize) 
         nearestNeighbours(pmap, pos, norm, (node << 1) + 1);
   }
   else {
      if (node << 1 < pmap -> heapSize) 
         nearestNeighbours(pmap, pos, norm, (node << 1) + 1);
      if (d2 < pmap -> maxDist && node << 1 <= pmap -> heapSize) 
         nearestNeighbours(pmap, pos, norm, node << 1);
   }
   /* Squared distance to current photon */
   dv [0] = pos [0] - p -> pos [0];
   dv [1] = pos [1] - p -> pos [1];
   dv [2] = pos [2] - p -> pos [2];
   d2 = DOT(dv, dv);
   /* Accept photon only if:
      (1) closer than current maximum dist,
      (2) has similar normal (ignored for volume photons) */
   if (d2 < pmap -> maxDist && (!norm || DOT(norm, p -> norm) > 0))
      if (pmap -> squeueEnd < sqSize) {
         /* Priority queue not full; append photon and restore heap */
         i = ++pmap -> squeueEnd;
         while (i > 1 && sq [(i >> 1) - 1].dist <= d2) {
            sq [i - 1].photon = sq [(i >> 1) - 1].photon;
            sq [i - 1].dist = sq [(i >> 1) - 1].dist;
            i >>= 1;
         }
         sq [--i].photon = p;
         sq [i].dist = d2;
         /* Update maxDist if we've just filled the queue */
         if (pmap -> squeueEnd >= pmap -> squeueSize)
            pmap -> maxDist = sq [0].dist;
      }
      else {
         /* Priority queue full; replace maximum, restore heap, and 
            update maxDist */
         i = 1;
         while (i <= sqSize >> 1) {
            j = i << 1;
            if (j < sqSize) if (sq [j - 1].dist < sq [j].dist) j++;
            if (d2 >= sq [j - 1].dist) break;
            sq [i - 1].photon = sq [j - 1].photon;
            sq [i - 1].dist = sq [j - 1].dist;
            i = j;
         }
         sq [--i].photon = p;
         sq [i].dist = d2;
         pmap -> maxDist = sq [0].dist;
      }
}



void findPhotons (PhotonMap* pmap, const RAY* ray)
{
   float pos [3], norm [3];
   register unsigned i;
   
   if (!pmap -> squeue) {
      /* Lazy init priority queue */
      pmap -> squeueSize = pmap -> maxGather + 1;
      pmap -> squeue = (PhotonSQNode*)malloc(pmap -> squeueSize * 
                                             sizeof(PhotonSQNode));
      if (!pmap -> squeue) 
         error(USER, "can't allocate photon priority queue");
      pmap -> minGathered = pmap -> maxGather;
      pmap -> maxGathered = pmap -> minGather;
      pmap -> totalGathered = 0;
      pmap -> minError = FHUGE;
      pmap -> maxError = -FHUGE;
      pmap -> rmsError = 0;
      /* Determine initial max search distance from minimum photon map
         irradiance */
      pmap -> maxInitDist = pmap -> squeueSize * 
                            colorAvg(pmap -> photonFlux) / 
                            (PI * pmap -> maxInitDist);
   }
   pmap -> squeueEnd = 0;
   pmap -> maxDist = pmap -> maxInitDist;
   /* Search position is ray -> rorg for volume photons, since we have no 
      intersection point. Normals are ignored -- these are incident 
      directions). */   
   if (pmap == volumePmap) {
      VCOPY(pos, ray -> rorg);
      nearestNeighbours(pmap, pos, NULL, 1);
   }
   else {
      VCOPY(pos, ray -> rop);
      VCOPY(norm, ray -> ron);
      nearestNeighbours(pmap, pos, norm, 1);
   }
#ifdef PMAP_SHORT_LOOKUP
   /* Gripe if fewer photons found than requested */
   if (pmap -> squeueEnd < pmap -> squeueSize)
      error(WARNING, "short photon map lookup");
#endif
}



static void nearestGlobal (const float pos [3], const float norm [3], 
                           Photon** photon, unsigned long node)
/* Recursive part of findGlobal(..).
   Note that all heap index handling is 1-based, but accesses to the 
   arrays are 0-based! */
{
   Photon *p = globalPmap -> heap + node - 1;
   /* Signed distance to current photon's splitting plane */
   register float d = pos [p -> discr] - p -> pos [p -> discr], 
                  d2 = d * d;
   float dv [3];
   
   /* Search subtree closer to pos first; exclude other subtree if the 
      distance to the splitting plane is greater than maxDist */
   if (d < 0) {
      if (node << 1 <= globalPmap -> heapSize) 
         nearestGlobal(pos, norm, photon, node << 1);
      if (d2 < globalPmap -> maxDist && 
          node << 1 < globalPmap -> heapSize) 
         nearestGlobal(pos, norm, photon, (node << 1) + 1);
   }
   else {
      if (node << 1 < globalPmap -> heapSize) 
         nearestGlobal(pos, norm, photon, (node << 1) + 1);
      if (d2 < globalPmap -> maxDist && 
          node << 1 <= globalPmap -> heapSize) 
         nearestGlobal(pos, norm, photon, node << 1);
   }
   /* Squared distance to current photon */
   dv [0] = pos [0] - p -> pos [0];
   dv [1] = pos [1] - p -> pos [1];
   dv [2] = pos [2] - p -> pos [2];
   d2 = DOT(dv, dv);
   if (d2 < globalPmap -> maxDist && DOT(norm, p -> norm) > 0) {
      /* Closest photon so far with similar normal */
      globalPmap -> maxDist = d2;
      *photon = p;
   }
}



Photon* findGlobal (const RAY* ray)
{
   float fpos [3], norm [3];
   register unsigned i;
   Photon* photon = NULL;

   VCOPY(fpos, ray -> rop);
   VCOPY(norm, ray -> ron);
   globalPmap -> maxDist = FHUGE;
   nearestGlobal(fpos, norm, &photon, 1);
   return photon;
}



static unsigned long medianPartition (const Photon* heap, 
                                      unsigned long* heapIdx,
                                      unsigned long* heapXdi, 
                                      unsigned long left, 
                                      unsigned long right, unsigned dim)
/* Returns index to median in heap from indices left to right 
   (inclusive) in dimension dim. The heap is partitioned relative to 
   median using a quicksort algorithm. The heap indices in heapIdx are
   sorted rather than the heap itself. */
{
   register const float* p;
   const unsigned long n = right - left + 1;
   register unsigned long l, r, lg2, n2, m;
   register unsigned d;
   
   /* Round down n to nearest power of 2 */
   for (lg2 = 0, n2 = n; n2 > 1; n2 >>= 1, ++lg2);
   n2 = 1 << lg2;
   /* Determine median position; this takes into account the fact that 
      only the last level in the heap can be partially empty, and that 
      it fills from left to right */
   m = left + ((n - n2) > (n2 >> 1) - 1 ? n2 - 1 : n - (n2 >> 1));
   while (right > left) {
      /* Pivot node */
      p = heap [heapIdx [right]].pos;
      l = left;
      r = right - 1;
      /* l & r converge, swapping elements out of order with respect to 
         pivot node. Identical keys are resolved by cycling through
         dim. The convergence point is then the pivot's position. */
      do {
         while (l <= r) {
            d = dim;
            while (heap [heapIdx [l]].pos [d] == p [d]) {
               d = (d + 1) % 3;
               if (d == dim) {
                  /* Ignore dupes? */
                  error(WARNING, "duplicate keys in photon heap");
                  l++;
                  break;
               }
            }
            if (heap [heapIdx [l]].pos [d] < p [d]) l++;
            else break;
         }
         while (r > l) {
            d = dim;
            while (heap [heapIdx [r]].pos [d] == p [d]) {
               d = (d + 1) % 3;
               if (d == dim) {
                  /* Ignore dupes? */
                  error(WARNING, "duplicate keys in photon heap");
                  r--;
                  break;
               }
            }
            if (heap [heapIdx [r]].pos [d] > p [d]) r--;
            else break;
         }
         /* Swap indices (not the nodes they point to) */
         n2 = heapIdx [l];
         heapIdx [l] = heapIdx [r];
         heapIdx [r] = n2;
         /* Update reverse indices */
         heapXdi [heapIdx [l]] = l;
         heapXdi [n2] = r;
      } while (l < r);
      /* Swap indices of convergence and pivot nodes */
      heapIdx [r] = heapIdx [l];
      heapIdx [l] = heapIdx [right];
      heapIdx [right] = n2;
      /* Update reverse indices */
      heapXdi [heapIdx [r]] = r;
      heapXdi [heapIdx [l]] = l;
      heapXdi [n2] = right;
      if (l >= m) right = l - 1;
      if (l <= m) left = l + 1;
   }
   /* Once left & right have converged at m, we have found the median */
   return m;
}



static void buildHeap (Photon* heap, unsigned long* heapIdx,
                       unsigned long* heapXdi, const float min [3], 
                       const float max [3], unsigned long left, 
                       unsigned long right, unsigned long root)
/* Recursive part of balancePhotons(..). Builds heap from subarray
   defined by indices left and right. min and max are the minimum resp. 
   maximum photon positions in the array. root is the index of the
   current subtree's root, which corresponds to the median's 1-based 
   index in the heap. heapIdx are the balanced heap indices. The heap
   is accessed indirectly through these. heapXdi are the reverse indices 
   from the heap to heapIdx so that heapXdi [heapIdx [i]] = i. */
{
   float maxLeft [3], minRight [3];
   Photon rootNode;
   unsigned d;
   /* Choose median for dimension with largest spread and partition 
      accordingly */
   const float d0 = max [0] - min [0], 
               d1 = max [1] - min [1], d2 = max [2] - min [2];
   const unsigned char dim = d0 > d1 ? d0 > d2 ? 0 : 2 : d1 > d2 ? 1 : 2;
   const unsigned long median = left == right 
                                ? left 
                                : medianPartition(heap, heapIdx, heapXdi, 
                                                  left, right, dim);
   
   /* Place median at root of current subtree. This consists of swapping 
      the median and the root nodes and updating the heap indices */
   memcpy(&rootNode, heap + heapIdx [median], sizeof(Photon));
   memcpy(heap + heapIdx [median], heap + root - 1, sizeof(Photon));
   rootNode.discr = dim;
   memcpy(heap + root - 1, &rootNode, sizeof(Photon));
   heapIdx [heapXdi [root - 1]] = heapIdx [median];
   heapXdi [heapIdx [median]] = heapXdi [root - 1];
   heapIdx [median] = root - 1;
   heapXdi [root - 1] = median;
   /* Update bounds for left and right subtrees and recurse on them */
   for (d = 0; d <= 2; d++)
      if (d == dim) 
         maxLeft [d] = minRight [d] = rootNode.pos [d];
      else {
         maxLeft [d] = max [d];
         minRight [d] = min [d];
      }
   if (left < median) 
      buildHeap(heap, heapIdx, heapXdi, min, maxLeft, 
                left, median - 1, root << 1);
   if (right > median) 
      buildHeap(heap, heapIdx, heapXdi, minRight, max, 
                median + 1, right, (root << 1) + 1);
}



void balancePhotons (PhotonMap* pmap, double photonFlux)
{
   Photon* heap = pmap -> heap;
   unsigned long i;
   unsigned long* heapIdx;        /* Photon index array */
   unsigned long* heapXdi;        /* Reverse index to heapIdx */
   COLOR flux;
   double avgFlux [3] = {0, 0, 0};
   
   if (pmap -> heapEnd) {
      pmap -> heapSize = pmap -> heapEnd;
      heapIdx = (unsigned long*)malloc(pmap -> heapSize * 
                                       sizeof(unsigned long));
      heapXdi = (unsigned long*)malloc(pmap -> heapSize * 
                                       sizeof(unsigned long));
      if (!heapIdx || !heapXdi)
         error(USER, "can't allocate heap index");
      for (i = 0; i < pmap -> heapSize; i++) {
         /* Initialize index arrays */
         heapXdi [i] = heapIdx [i] = i;
         getPhotonFlux(heap + i, flux);
         /* Scale photon's flux to photonFlux */
         scalecolor(flux, photonFlux);
         setPhotonFlux(heap + i, flux);
         /* Need a double here */
         addcolor(avgFlux, flux);
      }
      /* Average photon flux based on RGBE representation */
      scalecolor(avgFlux, 1.0 / pmap -> heapSize);
      copycolor(pmap -> photonFlux, avgFlux);
      /* Build kd-tree */
      buildHeap(pmap -> heap, heapIdx, heapXdi, pmap -> minPos, 
                pmap -> maxPos, 0, pmap -> heapSize - 1, 1);
      free(heapIdx);
      free(heapXdi);
   }
}



void deletePhotons (PhotonMap* pmap)
{
   free(pmap -> heap);
   free(pmap -> squeue);
   free(pmap -> biasCompHist);
   pmap -> heapSize = 0;
   pmap -> minGather = pmap -> maxGather =
      pmap -> squeueSize = pmap -> squeueEnd = 0;
}
