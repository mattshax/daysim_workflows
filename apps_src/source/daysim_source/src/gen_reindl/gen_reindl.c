/*	This program has been written by Oliver Walkenhorst at the
*	Fraunhofer Institute for Solar Energy Systems in Freiburg, Germany
*	last changes were added in January 2001
*/

#include  <stdio.h>
#include  <string.h>
#include  <math.h>
#include  <stdlib.h>
#include <strings.h>


char *header;
FILE *HEADER;               /*  header file  */
FILE *HOURLY_DATA;          /*  input weather data file  */
FILE *SHORT_TERM_DATA;      /*  input weather data shortterm file  */

/*  global variables for the header file key words and their default values */

char input_weather_data[200]="";
char input_weather_data_shortterm[200]="";   /*  default value: input_weather_data_shortterm = input_weather_data."shortterm_timestep"min  */
int shortterm_timestep=60;                /*  in minutes  */
int input_units_genshortterm;
int output_units_genshortterm=1;
int input_timestep=60;                /*  in minutes  */
int solar_time=0;                     /*  0=LST ; 1=solar time  */
long random_seed=-10;                 /*  seed for the pseudo-random-number generator, random_seed has to be a negative integer  */
int new=1;

/*  global variables for the header file key words representing station specific data  */

float latitude=45.32;
float longitude=75.67;
float time_zone=75.0;
float site_elevation=0.0;                   /*  in metres  */
float linke_turbidity_factor_am2[12];       /*  monthly means for jan-dec  */
char horizon_data_in[200];            /*  name of the horizon data file for the station where the input irradiance data were collected  */
                                      /*  (the file contains 36 horizon heights in degrees starting from N to E)                        */
char horizon_data_out[200];	      /*  name of the horizon data file for the location the output irradiance data are computed for    */
                                      /*  (the file contains 36 horizon heights in degrees starting from N to E)                        */

/*  other global variables  */

int sph=60;                               /*  sph=steps per hour: if shortterm_timestep < 60 1-min-data are generated  */
int horizon_in=0;                         /*  indicates if an input horizon data file is specified   */
int horizon_out=0;                        /*  indicates if an output horizon data file is specified  */
float horizon_azimuth_in[36];             /*  divide [-180�,180�] of the input horizon in 36 azimuth classes  */
                                          /*  (south=0�, horizon heights in degrees)                          */
float horizon_azimuth_out[36];            /*  divide [-180�,180�] of the output horizon in 36 azimuth classes */
                                          /*  (south=0�, horizon heights in degrees)                          */
int linke_estimation=1;                   /*  flag that indicates if estimation of the monthly linke factors is necessary  */

/*  constants used  */

const float Pi=3.14159265;
const float solar_constant_e = 1367.0;
const double DTR = 0.017453292; //Pi/180;
const double RTD = 57.2957795; //180/Pi;
const int F = sizeof(float);
const int I = sizeof(int);

/*  header files used  */

#include "file.h"
#include "file.c"
#include "sun.h"
#include "sun.c"
#include "nrutil.h"
#include "nrutil.c"
#include "numerical.h"
#include "numerical.c"
#include "skartveit.h"
#include "skartveit.c"

/*  main program  */

int main(int argc, char *argv[])
{
  int i;
  int month, day, jday;
  int *daylight_status;     /*  0=night hour, 1=sunrise/sunset hour, 2=innerday hour  */

  float time, *times;
  float irrad_glo, irrad_beam_nor, irrad_dif;     /* in W/m� */
  float *irrads_glo, *irrads_beam_nor , *irrads_dif, *indices_glo, *indices_beam, *sr_ss_indices_glo;
  float *irrads_glo_st, *irrads_glo_clear_st, *irrads_beam_nor_st, *irrads_dif_st, *indices_glo_st;
  float solar_elevation, solar_azimuth, eccentricity_correction;

void solar_elev_azi_ecc ( float latitude, float longitude, float time_zone, int jday, float time, int solar_time, float *solar_elevation, float *solar_azimuth, float *eccentricity_correction)

		/*  angles in degrees, times in hours  */
{
  float sol_time;
  float solar_declination, jday_angle;

 /*  solar elevation and azimuth formulae from sun.c  */
  if ( solar_time == 1 )   sol_time = time;
  if ( solar_time == 0 )   sol_time = time + 0.170 * sin( (4*Pi/373) * (jday - 80) ) - 0.129 * sin( (2*Pi/355) * (jday - 8) ) + 12/180.0 * (time_zone - longitude) ;

  solar_declination = RTD * 0.4093 * sin( (2*Pi/368) * (jday - 81) );
  jday_angle = 2*Pi*(jday - 1)/365;

  *solar_elevation = RTD * asin( sin(latitude*DTR) * sin(solar_declination*DTR) - cos(latitude*DTR) * cos(solar_declination*DTR) * cos(sol_time*(Pi/12)) );

  *solar_azimuth = RTD * ( -atan2( cos(solar_declination*DTR) * sin(sol_time*(Pi/12)),
 			           - cos(latitude*DTR)*sin(solar_declination*DTR) -
 			           sin(latitude*DTR)*cos(solar_declination*DTR)*cos(sol_time*(Pi/12)) ));

			  /*  eccentricity_correction formula used in genjdaylit.c */

  *eccentricity_correction = 1.00011 + 0.034221*cos(jday_angle)+0.00128*sin(jday_angle)+0.000719*cos(2*jday_angle)+0.000077*sin(2*jday_angle);
}

float diffuse_fraction ( float irrad_glo, float solar_elevation, float eccentricity_correction )
{
  /*  estimation of the diffuse fraction according to Reindl et al., Solar Energy, Vol.45, pp.1-7, 1990  = [Rei90]  */
  /*                        (reduced form without temperatures and humidities)                                                                          */

  float irrad_ex;
  float index_glo_ex;
  float dif_frac;

  if ( solar_elevation > 0 )  irrad_ex = solar_constant_e * eccentricity_correction * sin(DTR*solar_elevation);
  else irrad_ex = 0;

  if ( irrad_ex > 0 )   index_glo_ex = irrad_glo / irrad_ex;
  else  return 0;

  if ( index_glo_ex < 0 )  {  fprintf(stderr,"negative irrad_glo in diffuse_fraction_th\n"); exit(1);  }
  if ( index_glo_ex > 1 )  {  index_glo_ex = 1;  }

  if ( index_glo_ex <= 0.3 )
  {  dif_frac = 1.02 - 0.254*index_glo_ex + 0.0123*sin(DTR*solar_elevation);
     if ( dif_frac > 1 )  {  dif_frac = 1;  }
  }

  if ( index_glo_ex > 0.3 && index_glo_ex < 0.78 )
  {  dif_frac = 1.4 - 1.749*index_glo_ex + 0.177*sin(DTR*solar_elevation);
     if ( dif_frac > 0.97 )  {  dif_frac = 0.97;  }
     if ( dif_frac < 0.1 )   {  dif_frac = 0.1;  }
  }

  if ( index_glo_ex >= 0.78 )
  {  dif_frac = 0.486*index_glo_ex - 0.182*sin(DTR*solar_elevation);
     if ( dif_frac < 0.1 )  {  dif_frac = 0.1;  }
  }

  return dif_frac;
}


/* get the arguments */
	if (argc == 1)
	{
		fprintf(stdout,"\ngen_reindl: \n");
		printf("Program that transforms global irradiances into orizontal diffuse and direct normal irradiances \n");
		printf("Note that the -o option has to be specified!\n");
		printf("Note that the -i option has to be specified!\n");
		printf("\n");
		printf("Supported options are: \n");
		printf("-i\t input file [format: month day hour global_irradiation \n");
		printf("-o\t output file [format: month day hour dir_norm_irrad dif_hor_irrad \n");
		printf("-m\t time zone \n");
		printf("-l\t longitude [DEG, West is positive]\n");
		printf("-a\t latitude [DEG, North is positive]\n");
		exit(0);
	}
	for (i = 1; i < argc; i++)
		if (argv[i][0] == '-' )
			switch (argv[i][1])
			{
				case 'a':
					latitude=atof(argv[++i]);
					break;
				case 'm':
					time_zone=atof(argv[++i]);
					break;
				case 'l':
					longitude=atof(argv[++i]);
					break;

				case 'i':
					strcpy(input_weather_data, argv[++i]);
					break;
				case 'o':
					strcpy(input_weather_data_shortterm, argv[++i]);
					break;


			}


  HOURLY_DATA = open_input(input_weather_data);
  SHORT_TERM_DATA = open_output(input_weather_data_shortterm);

  if ( (times = malloc (24*F)) == NULL  )     {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (irrads_glo = malloc (24*F)) == NULL  )        {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (irrads_beam_nor = malloc (24*F)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (irrads_dif = malloc (24*F)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (indices_glo = malloc (24*F)) == NULL  )        {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (indices_beam = malloc (24*F)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (sr_ss_indices_glo = malloc (3*F)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (daylight_status = malloc (24*I)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }

  if ( (irrads_glo_st = malloc (sph*F)) == NULL  )        {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (irrads_glo_clear_st = malloc (sph*F)) == NULL  )        {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (irrads_beam_nor_st = malloc (sph*F)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (irrads_dif_st = malloc (sph*F)) == NULL  )   {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }
  if ( (indices_glo_st = malloc (sph*F)) == NULL  )      {   fprintf(stderr,"Out of memory in function main\n");  exit(1);  }



    for ( i=0 ; i<36 ; i++ )  horizon_azimuth_in[i]=0;


    // Question does Reindl need linke_turbidity_factor_am2[i] ?


    while( EOF != fscanf(HOURLY_DATA,"%d %d %f %f", &month, &day, &time, &irrad_glo))
    {
	 	jday=month_and_day_to_julian_day(month,day);
        if ( irrad_glo < 0 || irrad_glo > solar_constant_e )          /*  check irradiances and exit if necessary  */
         irrad_glo = solar_constant_e ;

		solar_elev_azi_ecc ( latitude, longitude, time_zone, jday, time, 0, &solar_elevation, &solar_azimuth, &eccentricity_correction);


		irrad_dif= diffuse_fraction(irrad_glo,solar_elevation,eccentricity_correction)*irrad_glo;

		if ( solar_elevation > 5.0 )
		{
		  irrad_beam_nor=(irrad_glo-irrad_dif)*1.0/sin(DTR*solar_elevation);
		}
		else
		{
	      irrad_beam_nor=0;
		  irrad_dif=irrad_glo;
		}
		if ( irrad_beam_nor > solar_constant_e )
		{
			irrad_beam_nor=solar_constant_e;
			irrad_dif=irrad_glo-irrad_beam_nor*sin(DTR*solar_elevation);
		}


        fprintf ( SHORT_TERM_DATA,"%d %d %.3f %.0f %.0f\n", month, day, time, irrad_beam_nor, irrad_dif );
	}

    close_file(HOURLY_DATA);
    close_file(SHORT_TERM_DATA);

  exit(0);

}
