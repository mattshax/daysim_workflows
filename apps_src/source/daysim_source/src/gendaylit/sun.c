/* Copyright (c) 1989 Regents of the University of California */
/* Copyright (c) 1994 ISE Freiburg */

/*
 *           SOLAR CALCULATIONS
 *
 *               3/31/87
 *
 */

#include  <stdlib.h>
#include  <math.h>
#include  "sun.h"

static	char *rcsid="$Header: /tmp_mnt/nfs/koll7/users/koll/jean/program/radiance/RAD/RCS/sun.c,v 1.1 94/05/17 19:23:08 jean Exp Locker: jean $";

double  s_latitude = 0.66;	/* site latitude (radians) */
double  s_longitude = 2.13;	/* site longitude (radians) */
double  s_meridian = 2.0944;	/* standard meridian (radians) */


int jdate( int month, int day)		/* Julian date (days into year) */
{
	static short  mo_da[12] = {0,31,59,90,120,151,181,212,243,273,304,334};
	
	return(mo_da[month-1] + day);
}


double stadj( int jd)		/* solar time adjustment from Julian date */
{
	return( 0.170 * sin( (4*M_PI/373) * (jd - 80) ) -
		0.129 * sin( (2*M_PI/355) * (jd - 8) ) +
		12 * (s_meridian - s_longitude) / M_PI );
}


double sdec( int jd)		/* solar declination angle from Julian date */
{
	return( 0.4093 * sin( (2*M_PI/368) * (jd - 81) ) );
}


double salt( double sd, double st)	/* solar altitude from solar declination and solar time */
{
	return( asin( sin(s_latitude) * sin(sd) -
			cos(s_latitude) * cos(sd) * cos(st*(M_PI/12)) ) );
}


double sazi( double sd,  double st)	/* solar azimuth from solar declination and solar time */
{
	return( -atan2( cos(sd)*sin(st*(M_PI/12)),
 			-cos(s_latitude)*sin(sd) -
 			sin(s_latitude)*cos(sd)*cos(st*(M_PI/12)) ) );
}
